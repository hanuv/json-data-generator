﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JsonTestData_Generator
{
    public partial class StartScreen : Form
    {
        public StartScreen()
        {
            InitializeComponent();
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            string filepath = "";

           DialogResult result =  folderBrowserDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                filepath = folderBrowserDialog1.SelectedPath.ToString();
            }

            txtFileCreationPath.Text = filepath;
        }

        private void button2_Click(object sender, EventArgs e)
        {



        }
    }
}
