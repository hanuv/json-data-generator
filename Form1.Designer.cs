﻿namespace JsonTestData_Generator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.btnGenerateJSON = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.txtFileCreationPath = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPerson = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtStreetName = new System.Windows.Forms.TextBox();
            this.txtHouseNumber = new System.Windows.Forms.TextBox();
            this.txtSuffix = new System.Windows.Forms.TextBox();
            this.txtPostalCode = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dpDivorceDate = new System.Windows.Forms.DateTimePicker();
            this.dpDateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.cbPersonType = new System.Windows.Forms.ComboBox();
            this.cbGender = new System.Windows.Forms.ComboBox();
            this.cbMaritalStatus = new System.Windows.Forms.ComboBox();
            this.cbEverDivorced = new System.Windows.Forms.ComboBox();
            this.cbSmoker = new System.Windows.Forms.ComboBox();
            this.txtBirthSurname = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtSocialSecurityNumber = new System.Windows.Forms.TextBox();
            this.txtDiscountAOWYears = new System.Windows.Forms.TextBox();
            this.txtNationality = new System.Windows.Forms.TextBox();
            this.txtInitials = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtPrefix = new System.Windows.Forms.TextBox();
            this.txtPersonID = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabIncomes = new System.Windows.Forms.TabPage();
            this.gbYearlyIncomes = new System.Windows.Forms.GroupBox();
            this.cbNoYearlyIncomesData = new System.Windows.Forms.CheckBox();
            this.txtYearlyIncomeValue = new System.Windows.Forms.TextBox();
            this.txtYearlyIncomeProvisionValue = new System.Windows.Forms.TextBox();
            this.txtYearlyIncomeExtraMonth = new System.Windows.Forms.TextBox();
            this.txtYearlyIncomeIrregularityPayment = new System.Windows.Forms.TextBox();
            this.txtYEarlyIncomeOvertimeValue = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtYearlyIncomeEndOfYearBenefit = new System.Windows.Forms.TextBox();
            this.txtYearlyIncomeHolidayAllowanceValue = new System.Windows.Forms.TextBox();
            this.txtYEarlyIncomeYEar = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.gbIncomes = new System.Windows.Forms.GroupBox();
            this.cbNoIncomesData = new System.Windows.Forms.CheckBox();
            this.dpIncomeEndDate = new System.Windows.Forms.DateTimePicker();
            this.dpIncomeStartDate = new System.Windows.Forms.DateTimePicker();
            this.cbIncomesType = new System.Windows.Forms.ComboBox();
            this.txtIncomesPArtTimePercentage = new System.Windows.Forms.TextBox();
            this.txtIncomesOwner = new System.Windows.Forms.TextBox();
            this.txtIncomesID = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.tabPensions = new System.Windows.Forms.TabPage();
            this.gbPensions = new System.Windows.Forms.GroupBox();
            this.cbNoPensionData = new System.Windows.Forms.CheckBox();
            this.dpPensionStartDate = new System.Windows.Forms.DateTimePicker();
            this.cbPensionType = new System.Windows.Forms.ComboBox();
            this.txtPensionValue = new System.Windows.Forms.TextBox();
            this.txtPensionOwner = new System.Windows.Forms.TextBox();
            this.txtPensionID = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.tabExpenses = new System.Windows.Forms.TabPage();
            this.gbExpenses = new System.Windows.Forms.GroupBox();
            this.cbNoExpensesData = new System.Windows.Forms.CheckBox();
            this.cbExpenseType = new System.Windows.Forms.ComboBox();
            this.txtExpenseMade = new System.Windows.Forms.TextBox();
            this.txtExpenseOwner = new System.Windows.Forms.TextBox();
            this.txtExpenseID = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.tabAssets = new System.Windows.Forms.TabPage();
            this.gbAssets = new System.Windows.Forms.GroupBox();
            this.cbNoAssetsData = new System.Windows.Forms.CheckBox();
            this.txtAssetInterestRate = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.cbAssetType = new System.Windows.Forms.ComboBox();
            this.txtAssetValue = new System.Windows.Forms.TextBox();
            this.txtAssetOwner = new System.Windows.Forms.TextBox();
            this.txtAssetID = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.tabLoans = new System.Windows.Forms.TabPage();
            this.gbLoans = new System.Windows.Forms.GroupBox();
            this.cbNoLoansData = new System.Windows.Forms.CheckBox();
            this.cbLoanType = new System.Windows.Forms.ComboBox();
            this.txtLoanAmount = new System.Windows.Forms.TextBox();
            this.txtLoanOwner = new System.Windows.Forms.TextBox();
            this.txtLoanID = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.tabRealProperty = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbNoLeaseHoldData = new System.Windows.Forms.CheckBox();
            this.cbLeaseType = new System.Windows.Forms.ComboBox();
            this.dpLeaseHoldEndDate = new System.Windows.Forms.DateTimePicker();
            this.txtLeaseHoldID = new System.Windows.Forms.TextBox();
            this.txtLeaseHoldValue = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.gbRealPropertyHouse = new System.Windows.Forms.GroupBox();
            this.cbNoHouseData = new System.Windows.Forms.CheckBox();
            this.cbHouseObjectType = new System.Windows.Forms.ComboBox();
            this.label94 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.txtHouseStreetName = new System.Windows.Forms.TextBox();
            this.txtHouseExtraWorkContractor = new System.Windows.Forms.TextBox();
            this.txtHouseHouseNumber = new System.Windows.Forms.TextBox();
            this.txtHousePostalCode = new System.Windows.Forms.TextBox();
            this.dpHousePurchaseDate = new System.Windows.Forms.DateTimePicker();
            this.txtHouseCity = new System.Windows.Forms.TextBox();
            this.txtContractorSum = new System.Windows.Forms.TextBox();
            this.txtHouseMunicipality = new System.Windows.Forms.TextBox();
            this.txtHouseOverdueMaintenance = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.txtHouseEconomicValue = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.txtHouseEcoValueAfterImprovements = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.txtHouseGroundPrice = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.cbHouseType = new System.Windows.Forms.ComboBox();
            this.txtHouseProjectName = new System.Windows.Forms.TextBox();
            this.txtHouseProjectNumber = new System.Windows.Forms.TextBox();
            this.txtHouseID = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.tabFinanceConstruction = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbNoFinanceConstructionData = new System.Windows.Forms.CheckBox();
            this.dpMortgageStartDate = new System.Windows.Forms.DateTimePicker();
            this.txtMortgageTotal = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.txtMortgageRegistration = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.txtMortGageDeed = new System.Windows.Forms.TextBox();
            this.txtAdviceFee = new System.Windows.Forms.TextBox();
            this.txtMarketValueReport = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.tabLoanParts = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cbNoLoanParts = new System.Windows.Forms.CheckBox();
            this.txtLoanPartID = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.cbLoanPartsType = new System.Windows.Forms.ComboBox();
            this.dpLoanpartsEndDate = new System.Windows.Forms.DateTimePicker();
            this.txtLoanpartsFixedInterestDuration = new System.Windows.Forms.TextBox();
            this.txtLoanPartsValue = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.tabTermLifeInsurances = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txttliInitialValue = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.cbNoTermLifeInsurance = new System.Windows.Forms.CheckBox();
            this.txtTLIForLoanPArt = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.txtTLIInsuredValue = new System.Windows.Forms.TextBox();
            this.txtTLIPayment = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.txtTLIOwner = new System.Windows.Forms.TextBox();
            this.txtTLIID = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.tabApplicationDetails = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.cbNoApplicationDetails = new System.Windows.Forms.CheckBox();
            this.cbApplicationType = new System.Windows.Forms.ComboBox();
            this.txtAdviceStatus = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.cbSaveInputData = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabPerson.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabIncomes.SuspendLayout();
            this.gbYearlyIncomes.SuspendLayout();
            this.gbIncomes.SuspendLayout();
            this.tabPensions.SuspendLayout();
            this.gbPensions.SuspendLayout();
            this.tabExpenses.SuspendLayout();
            this.gbExpenses.SuspendLayout();
            this.tabAssets.SuspendLayout();
            this.gbAssets.SuspendLayout();
            this.tabLoans.SuspendLayout();
            this.gbLoans.SuspendLayout();
            this.tabRealProperty.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.gbRealPropertyHouse.SuspendLayout();
            this.tabFinanceConstruction.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabLoanParts.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabTermLifeInsurances.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabApplicationDetails.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGenerateJSON
            // 
            this.btnGenerateJSON.Location = new System.Drawing.Point(499, 621);
            this.btnGenerateJSON.Name = "btnGenerateJSON";
            this.btnGenerateJSON.Size = new System.Drawing.Size(205, 23);
            this.btnGenerateJSON.TabIndex = 0;
            this.btnGenerateJSON.Text = "Generate JSON";
            this.btnGenerateJSON.UseVisualStyleBackColor = true;
            this.btnGenerateJSON.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPerson);
            this.tabControl1.Controls.Add(this.tabIncomes);
            this.tabControl1.Controls.Add(this.tabPensions);
            this.tabControl1.Controls.Add(this.tabExpenses);
            this.tabControl1.Controls.Add(this.tabAssets);
            this.tabControl1.Controls.Add(this.tabLoans);
            this.tabControl1.Controls.Add(this.tabRealProperty);
            this.tabControl1.Controls.Add(this.tabFinanceConstruction);
            this.tabControl1.Controls.Add(this.tabLoanParts);
            this.tabControl1.Controls.Add(this.tabTermLifeInsurances);
            this.tabControl1.Controls.Add(this.tabApplicationDetails);
            this.tabControl1.Location = new System.Drawing.Point(30, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1233, 603);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1225, 574);
            this.tabPage1.TabIndex = 11;
            this.tabPage1.Text = "Configure";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Silver;
            this.groupBox8.Controls.Add(this.cbSaveInputData);
            this.groupBox8.Controls.Add(this.txtFileCreationPath);
            this.groupBox8.Controls.Add(this.button1);
            this.groupBox8.Location = new System.Drawing.Point(6, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(1213, 562);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Configure";
            // 
            // txtFileCreationPath
            // 
            this.txtFileCreationPath.Location = new System.Drawing.Point(317, 229);
            this.txtFileCreationPath.Name = "txtFileCreationPath";
            this.txtFileCreationPath.Size = new System.Drawing.Size(360, 22);
            this.txtFileCreationPath.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(683, 217);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 47);
            this.button1.TabIndex = 3;
            this.button1.Text = "Set Folder";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // tabPerson
            // 
            this.tabPerson.Controls.Add(this.groupBox2);
            this.tabPerson.Controls.Add(this.groupBox1);
            this.tabPerson.Location = new System.Drawing.Point(4, 25);
            this.tabPerson.Name = "tabPerson";
            this.tabPerson.Padding = new System.Windows.Forms.Padding(3);
            this.tabPerson.Size = new System.Drawing.Size(1225, 574);
            this.tabPerson.TabIndex = 0;
            this.tabPerson.Text = "Person";
            this.tabPerson.UseVisualStyleBackColor = true;
            this.tabPerson.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Silver;
            this.groupBox2.Controls.Add(this.txtStreetName);
            this.groupBox2.Controls.Add(this.txtHouseNumber);
            this.groupBox2.Controls.Add(this.txtSuffix);
            this.groupBox2.Controls.Add(this.txtPostalCode);
            this.groupBox2.Controls.Add(this.txtCity);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Location = new System.Drawing.Point(822, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(397, 562);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Address Data";
            // 
            // txtStreetName
            // 
            this.txtStreetName.Location = new System.Drawing.Point(137, 51);
            this.txtStreetName.Multiline = true;
            this.txtStreetName.Name = "txtStreetName";
            this.txtStreetName.Size = new System.Drawing.Size(245, 22);
            this.txtStreetName.TabIndex = 30;
            // 
            // txtHouseNumber
            // 
            this.txtHouseNumber.Location = new System.Drawing.Point(137, 84);
            this.txtHouseNumber.Multiline = true;
            this.txtHouseNumber.Name = "txtHouseNumber";
            this.txtHouseNumber.Size = new System.Drawing.Size(245, 22);
            this.txtHouseNumber.TabIndex = 29;
            // 
            // txtSuffix
            // 
            this.txtSuffix.Location = new System.Drawing.Point(137, 116);
            this.txtSuffix.Multiline = true;
            this.txtSuffix.Name = "txtSuffix";
            this.txtSuffix.Size = new System.Drawing.Size(245, 22);
            this.txtSuffix.TabIndex = 28;
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Location = new System.Drawing.Point(137, 147);
            this.txtPostalCode.Multiline = true;
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Size = new System.Drawing.Size(245, 22);
            this.txtPostalCode.TabIndex = 27;
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(137, 183);
            this.txtCity.Multiline = true;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(245, 22);
            this.txtCity.TabIndex = 26;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(30, 191);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(31, 17);
            this.label24.TabIndex = 25;
            this.label24.Text = "City";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(30, 51);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(87, 17);
            this.label20.TabIndex = 24;
            this.label20.Text = "Street Name";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(30, 84);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(103, 17);
            this.label21.TabIndex = 23;
            this.label21.Text = "House Number";
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(30, 154);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(84, 17);
            this.label22.TabIndex = 21;
            this.label22.Text = "Postal Code";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(30, 116);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 17);
            this.label23.TabIndex = 22;
            this.label23.Text = "Suffix";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Silver;
            this.groupBox1.Controls.Add(this.dpDivorceDate);
            this.groupBox1.Controls.Add(this.dpDateOfBirth);
            this.groupBox1.Controls.Add(this.cbPersonType);
            this.groupBox1.Controls.Add(this.cbGender);
            this.groupBox1.Controls.Add(this.cbMaritalStatus);
            this.groupBox1.Controls.Add(this.cbEverDivorced);
            this.groupBox1.Controls.Add(this.cbSmoker);
            this.groupBox1.Controls.Add(this.txtBirthSurname);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.txtSocialSecurityNumber);
            this.groupBox1.Controls.Add(this.txtDiscountAOWYears);
            this.groupBox1.Controls.Add(this.txtNationality);
            this.groupBox1.Controls.Add(this.txtInitials);
            this.groupBox1.Controls.Add(this.txtFirstName);
            this.groupBox1.Controls.Add(this.txtLastName);
            this.groupBox1.Controls.Add(this.txtPrefix);
            this.groupBox1.Controls.Add(this.txtPersonID);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(19, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(797, 562);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Personal Data";
            // 
            // dpDivorceDate
            // 
            this.dpDivorceDate.Location = new System.Drawing.Point(106, 483);
            this.dpDivorceDate.Name = "dpDivorceDate";
            this.dpDivorceDate.Size = new System.Drawing.Size(262, 22);
            this.dpDivorceDate.TabIndex = 35;
            // 
            // dpDateOfBirth
            // 
            this.dpDateOfBirth.Location = new System.Drawing.Point(106, 367);
            this.dpDateOfBirth.Name = "dpDateOfBirth";
            this.dpDateOfBirth.Size = new System.Drawing.Size(262, 22);
            this.dpDateOfBirth.TabIndex = 34;
            // 
            // cbPersonType
            // 
            this.cbPersonType.FormattingEnabled = true;
            this.cbPersonType.Location = new System.Drawing.Point(106, 86);
            this.cbPersonType.MaxDropDownItems = 20;
            this.cbPersonType.Name = "cbPersonType";
            this.cbPersonType.Size = new System.Drawing.Size(262, 24);
            this.cbPersonType.Sorted = true;
            this.cbPersonType.TabIndex = 33;
            // 
            // cbGender
            // 
            this.cbGender.FormattingEnabled = true;
            this.cbGender.Location = new System.Drawing.Point(106, 323);
            this.cbGender.Name = "cbGender";
            this.cbGender.Size = new System.Drawing.Size(262, 24);
            this.cbGender.TabIndex = 32;
            // 
            // cbMaritalStatus
            // 
            this.cbMaritalStatus.FormattingEnabled = true;
            this.cbMaritalStatus.Location = new System.Drawing.Point(106, 404);
            this.cbMaritalStatus.Name = "cbMaritalStatus";
            this.cbMaritalStatus.Size = new System.Drawing.Size(262, 24);
            this.cbMaritalStatus.TabIndex = 30;
            // 
            // cbEverDivorced
            // 
            this.cbEverDivorced.FormattingEnabled = true;
            this.cbEverDivorced.Location = new System.Drawing.Point(106, 447);
            this.cbEverDivorced.Name = "cbEverDivorced";
            this.cbEverDivorced.Size = new System.Drawing.Size(262, 24);
            this.cbEverDivorced.TabIndex = 29;
            // 
            // cbSmoker
            // 
            this.cbSmoker.FormattingEnabled = true;
            this.cbSmoker.Location = new System.Drawing.Point(580, 115);
            this.cbSmoker.Name = "cbSmoker";
            this.cbSmoker.Size = new System.Drawing.Size(181, 24);
            this.cbSmoker.TabIndex = 27;
            // 
            // txtBirthSurname
            // 
            this.txtBirthSurname.Location = new System.Drawing.Point(106, 260);
            this.txtBirthSurname.Multiline = true;
            this.txtBirthSurname.Name = "txtBirthSurname";
            this.txtBirthSurname.Size = new System.Drawing.Size(262, 22);
            this.txtBirthSurname.TabIndex = 26;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(106, 295);
            this.txtEmail.Multiline = true;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(262, 22);
            this.txtEmail.TabIndex = 25;
            // 
            // txtSocialSecurityNumber
            // 
            this.txtSocialSecurityNumber.Location = new System.Drawing.Point(580, 50);
            this.txtSocialSecurityNumber.Multiline = true;
            this.txtSocialSecurityNumber.Name = "txtSocialSecurityNumber";
            this.txtSocialSecurityNumber.Size = new System.Drawing.Size(181, 22);
            this.txtSocialSecurityNumber.TabIndex = 24;
            // 
            // txtDiscountAOWYears
            // 
            this.txtDiscountAOWYears.Location = new System.Drawing.Point(580, 80);
            this.txtDiscountAOWYears.Multiline = true;
            this.txtDiscountAOWYears.Name = "txtDiscountAOWYears";
            this.txtDiscountAOWYears.Size = new System.Drawing.Size(181, 22);
            this.txtDiscountAOWYears.TabIndex = 23;
            // 
            // txtNationality
            // 
            this.txtNationality.Location = new System.Drawing.Point(580, 151);
            this.txtNationality.Multiline = true;
            this.txtNationality.Name = "txtNationality";
            this.txtNationality.Size = new System.Drawing.Size(181, 22);
            this.txtNationality.TabIndex = 22;
            // 
            // txtInitials
            // 
            this.txtInitials.Location = new System.Drawing.Point(106, 124);
            this.txtInitials.Multiline = true;
            this.txtInitials.Name = "txtInitials";
            this.txtInitials.Size = new System.Drawing.Size(262, 22);
            this.txtInitials.TabIndex = 21;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(106, 156);
            this.txtFirstName.Multiline = true;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(262, 22);
            this.txtFirstName.TabIndex = 20;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(106, 188);
            this.txtLastName.Multiline = true;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(262, 22);
            this.txtLastName.TabIndex = 19;
            // 
            // txtPrefix
            // 
            this.txtPrefix.Location = new System.Drawing.Point(106, 221);
            this.txtPrefix.Multiline = true;
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(262, 22);
            this.txtPrefix.TabIndex = 18;
            // 
            // txtPersonID
            // 
            this.txtPersonID.Location = new System.Drawing.Point(106, 50);
            this.txtPersonID.Multiline = true;
            this.txtPersonID.Name = "txtPersonID";
            this.txtPersonID.Size = new System.Drawing.Size(262, 22);
            this.txtPersonID.TabIndex = 17;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(403, 53);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(155, 17);
            this.label17.TabIndex = 16;
            this.label17.Text = "Social Security Number";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(403, 80);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(141, 17);
            this.label16.TabIndex = 15;
            this.label16.Text = "Discount AOW Years";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 488);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(90, 17);
            this.label13.TabIndex = 12;
            this.label13.Text = "Divorce Date";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(403, 156);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 17);
            this.label14.TabIndex = 13;
            this.label14.Text = "Nationality";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 447);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 17);
            this.label12.TabIndex = 11;
            this.label12.Text = "Ever Divorced";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(403, 120);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 17);
            this.label15.TabIndex = 14;
            this.label15.Text = "Smoker";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 407);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 17);
            this.label11.TabIndex = 10;
            this.label11.Text = "Marital Status";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "PersonID";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 367);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 17);
            this.label10.TabIndex = 9;
            this.label10.Text = "Date of Birth";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Person Type";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 330);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 17);
            this.label9.TabIndex = 8;
            this.label9.Text = "Gender";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Initials";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 295);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Email";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "First Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 260);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Birth Surname";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Last Name";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 226);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Prefix";
            // 
            // tabIncomes
            // 
            this.tabIncomes.Controls.Add(this.gbYearlyIncomes);
            this.tabIncomes.Controls.Add(this.gbIncomes);
            this.tabIncomes.Location = new System.Drawing.Point(4, 25);
            this.tabIncomes.Name = "tabIncomes";
            this.tabIncomes.Padding = new System.Windows.Forms.Padding(3);
            this.tabIncomes.Size = new System.Drawing.Size(1225, 574);
            this.tabIncomes.TabIndex = 1;
            this.tabIncomes.Text = "Incomes";
            this.tabIncomes.UseVisualStyleBackColor = true;
            // 
            // gbYearlyIncomes
            // 
            this.gbYearlyIncomes.BackColor = System.Drawing.Color.Silver;
            this.gbYearlyIncomes.Controls.Add(this.cbNoYearlyIncomesData);
            this.gbYearlyIncomes.Controls.Add(this.txtYearlyIncomeValue);
            this.gbYearlyIncomes.Controls.Add(this.txtYearlyIncomeProvisionValue);
            this.gbYearlyIncomes.Controls.Add(this.txtYearlyIncomeExtraMonth);
            this.gbYearlyIncomes.Controls.Add(this.txtYearlyIncomeIrregularityPayment);
            this.gbYearlyIncomes.Controls.Add(this.txtYEarlyIncomeOvertimeValue);
            this.gbYearlyIncomes.Controls.Add(this.label37);
            this.gbYearlyIncomes.Controls.Add(this.txtYearlyIncomeEndOfYearBenefit);
            this.gbYearlyIncomes.Controls.Add(this.txtYearlyIncomeHolidayAllowanceValue);
            this.gbYearlyIncomes.Controls.Add(this.txtYEarlyIncomeYEar);
            this.gbYearlyIncomes.Controls.Add(this.label18);
            this.gbYearlyIncomes.Controls.Add(this.label19);
            this.gbYearlyIncomes.Controls.Add(this.label25);
            this.gbYearlyIncomes.Controls.Add(this.label27);
            this.gbYearlyIncomes.Controls.Add(this.label29);
            this.gbYearlyIncomes.Controls.Add(this.label31);
            this.gbYearlyIncomes.Controls.Add(this.label36);
            this.gbYearlyIncomes.Location = new System.Drawing.Point(615, 6);
            this.gbYearlyIncomes.Name = "gbYearlyIncomes";
            this.gbYearlyIncomes.Size = new System.Drawing.Size(594, 550);
            this.gbYearlyIncomes.TabIndex = 1;
            this.gbYearlyIncomes.TabStop = false;
            this.gbYearlyIncomes.Text = "Yearly Incomes";
            this.gbYearlyIncomes.Enter += new System.EventHandler(this.gbYearlyIncomes_Enter);
            // 
            // cbNoYearlyIncomesData
            // 
            this.cbNoYearlyIncomesData.AutoSize = true;
            this.cbNoYearlyIncomesData.Location = new System.Drawing.Point(70, 63);
            this.cbNoYearlyIncomesData.Name = "cbNoYearlyIncomesData";
            this.cbNoYearlyIncomesData.Size = new System.Drawing.Size(182, 21);
            this.cbNoYearlyIncomesData.TabIndex = 72;
            this.cbNoYearlyIncomesData.Text = "No Yearly Incomes Data";
            this.cbNoYearlyIncomesData.UseVisualStyleBackColor = true;
            this.cbNoYearlyIncomesData.CheckStateChanged += new System.EventHandler(this.cbNoYearlyIncomesData_CheckStateChanged);
            // 
            // txtYearlyIncomeValue
            // 
            this.txtYearlyIncomeValue.Location = new System.Drawing.Point(250, 188);
            this.txtYearlyIncomeValue.Multiline = true;
            this.txtYearlyIncomeValue.Name = "txtYearlyIncomeValue";
            this.txtYearlyIncomeValue.Size = new System.Drawing.Size(181, 22);
            this.txtYearlyIncomeValue.TabIndex = 70;
            // 
            // txtYearlyIncomeProvisionValue
            // 
            this.txtYearlyIncomeProvisionValue.Location = new System.Drawing.Point(250, 323);
            this.txtYearlyIncomeProvisionValue.Multiline = true;
            this.txtYearlyIncomeProvisionValue.Name = "txtYearlyIncomeProvisionValue";
            this.txtYearlyIncomeProvisionValue.Size = new System.Drawing.Size(181, 22);
            this.txtYearlyIncomeProvisionValue.TabIndex = 69;
            // 
            // txtYearlyIncomeExtraMonth
            // 
            this.txtYearlyIncomeExtraMonth.Location = new System.Drawing.Point(250, 290);
            this.txtYearlyIncomeExtraMonth.Multiline = true;
            this.txtYearlyIncomeExtraMonth.Name = "txtYearlyIncomeExtraMonth";
            this.txtYearlyIncomeExtraMonth.Size = new System.Drawing.Size(181, 22);
            this.txtYearlyIncomeExtraMonth.TabIndex = 68;
            // 
            // txtYearlyIncomeIrregularityPayment
            // 
            this.txtYearlyIncomeIrregularityPayment.Location = new System.Drawing.Point(250, 256);
            this.txtYearlyIncomeIrregularityPayment.Multiline = true;
            this.txtYearlyIncomeIrregularityPayment.Name = "txtYearlyIncomeIrregularityPayment";
            this.txtYearlyIncomeIrregularityPayment.Size = new System.Drawing.Size(181, 22);
            this.txtYearlyIncomeIrregularityPayment.TabIndex = 67;
            // 
            // txtYEarlyIncomeOvertimeValue
            // 
            this.txtYEarlyIncomeOvertimeValue.Location = new System.Drawing.Point(250, 394);
            this.txtYEarlyIncomeOvertimeValue.Multiline = true;
            this.txtYEarlyIncomeOvertimeValue.Name = "txtYEarlyIncomeOvertimeValue";
            this.txtYEarlyIncomeOvertimeValue.Size = new System.Drawing.Size(181, 22);
            this.txtYEarlyIncomeOvertimeValue.TabIndex = 66;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(67, 394);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(105, 17);
            this.label37.TabIndex = 65;
            this.label37.Text = "Overtime Value";
            // 
            // txtYearlyIncomeEndOfYearBenefit
            // 
            this.txtYearlyIncomeEndOfYearBenefit.Location = new System.Drawing.Point(250, 357);
            this.txtYearlyIncomeEndOfYearBenefit.Multiline = true;
            this.txtYearlyIncomeEndOfYearBenefit.Name = "txtYearlyIncomeEndOfYearBenefit";
            this.txtYearlyIncomeEndOfYearBenefit.Size = new System.Drawing.Size(181, 22);
            this.txtYearlyIncomeEndOfYearBenefit.TabIndex = 56;
            // 
            // txtYearlyIncomeHolidayAllowanceValue
            // 
            this.txtYearlyIncomeHolidayAllowanceValue.Location = new System.Drawing.Point(250, 224);
            this.txtYearlyIncomeHolidayAllowanceValue.Multiline = true;
            this.txtYearlyIncomeHolidayAllowanceValue.Name = "txtYearlyIncomeHolidayAllowanceValue";
            this.txtYearlyIncomeHolidayAllowanceValue.Size = new System.Drawing.Size(181, 22);
            this.txtYearlyIncomeHolidayAllowanceValue.TabIndex = 54;
            // 
            // txtYEarlyIncomeYEar
            // 
            this.txtYEarlyIncomeYEar.Location = new System.Drawing.Point(250, 150);
            this.txtYEarlyIncomeYEar.Multiline = true;
            this.txtYEarlyIncomeYEar.Name = "txtYEarlyIncomeYEar";
            this.txtYEarlyIncomeYEar.Size = new System.Drawing.Size(181, 22);
            this.txtYEarlyIncomeYEar.TabIndex = 50;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(67, 150);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(38, 17);
            this.label18.TabIndex = 37;
            this.label18.Text = "Year";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(67, 190);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 17);
            this.label19.TabIndex = 38;
            this.label19.Text = "Value";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(67, 224);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(162, 17);
            this.label25.TabIndex = 39;
            this.label25.Text = "Holiday Allowance Value";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(67, 256);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(134, 17);
            this.label27.TabIndex = 40;
            this.label27.Text = "Irregularity Payment";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(67, 357);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(171, 17);
            this.label29.TabIndex = 43;
            this.label29.Text = "End of Year Benefit Value";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(67, 285);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(83, 17);
            this.label31.TabIndex = 41;
            this.label31.Text = "Extra Month";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(67, 323);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(106, 17);
            this.label36.TabIndex = 42;
            this.label36.Text = "Provision Value";
            // 
            // gbIncomes
            // 
            this.gbIncomes.BackColor = System.Drawing.Color.Silver;
            this.gbIncomes.Controls.Add(this.cbNoIncomesData);
            this.gbIncomes.Controls.Add(this.dpIncomeEndDate);
            this.gbIncomes.Controls.Add(this.dpIncomeStartDate);
            this.gbIncomes.Controls.Add(this.cbIncomesType);
            this.gbIncomes.Controls.Add(this.txtIncomesPArtTimePercentage);
            this.gbIncomes.Controls.Add(this.txtIncomesOwner);
            this.gbIncomes.Controls.Add(this.txtIncomesID);
            this.gbIncomes.Controls.Add(this.label26);
            this.gbIncomes.Controls.Add(this.label28);
            this.gbIncomes.Controls.Add(this.label30);
            this.gbIncomes.Controls.Add(this.label33);
            this.gbIncomes.Controls.Add(this.label34);
            this.gbIncomes.Controls.Add(this.label35);
            this.gbIncomes.Location = new System.Drawing.Point(6, 6);
            this.gbIncomes.Name = "gbIncomes";
            this.gbIncomes.Size = new System.Drawing.Size(603, 550);
            this.gbIncomes.TabIndex = 0;
            this.gbIncomes.TabStop = false;
            this.gbIncomes.Text = "Incomes";
            // 
            // cbNoIncomesData
            // 
            this.cbNoIncomesData.AutoSize = true;
            this.cbNoIncomesData.Location = new System.Drawing.Point(24, 73);
            this.cbNoIncomesData.Name = "cbNoIncomesData";
            this.cbNoIncomesData.Size = new System.Drawing.Size(138, 21);
            this.cbNoIncomesData.TabIndex = 66;
            this.cbNoIncomesData.Text = "No Incomes Data";
            this.cbNoIncomesData.UseVisualStyleBackColor = true;
            this.cbNoIncomesData.CheckStateChanged += new System.EventHandler(this.cbNoIncomesData_CheckStateChanged);
            // 
            // dpIncomeEndDate
            // 
            this.dpIncomeEndDate.Location = new System.Drawing.Point(173, 289);
            this.dpIncomeEndDate.Name = "dpIncomeEndDate";
            this.dpIncomeEndDate.Size = new System.Drawing.Size(258, 22);
            this.dpIncomeEndDate.TabIndex = 64;
            // 
            // dpIncomeStartDate
            // 
            this.dpIncomeStartDate.Location = new System.Drawing.Point(173, 256);
            this.dpIncomeStartDate.Name = "dpIncomeStartDate";
            this.dpIncomeStartDate.Size = new System.Drawing.Size(258, 22);
            this.dpIncomeStartDate.TabIndex = 62;
            // 
            // cbIncomesType
            // 
            this.cbIncomesType.FormattingEnabled = true;
            this.cbIncomesType.Location = new System.Drawing.Point(173, 186);
            this.cbIncomesType.Name = "cbIncomesType";
            this.cbIncomesType.Size = new System.Drawing.Size(258, 24);
            this.cbIncomesType.TabIndex = 60;
            // 
            // txtIncomesPArtTimePercentage
            // 
            this.txtIncomesPArtTimePercentage.Location = new System.Drawing.Point(173, 325);
            this.txtIncomesPArtTimePercentage.Multiline = true;
            this.txtIncomesPArtTimePercentage.Name = "txtIncomesPArtTimePercentage";
            this.txtIncomesPArtTimePercentage.Size = new System.Drawing.Size(258, 22);
            this.txtIncomesPArtTimePercentage.TabIndex = 56;
            // 
            // txtIncomesOwner
            // 
            this.txtIncomesOwner.Location = new System.Drawing.Point(173, 224);
            this.txtIncomesOwner.Multiline = true;
            this.txtIncomesOwner.Name = "txtIncomesOwner";
            this.txtIncomesOwner.Size = new System.Drawing.Size(258, 22);
            this.txtIncomesOwner.TabIndex = 54;
            // 
            // txtIncomesID
            // 
            this.txtIncomesID.Location = new System.Drawing.Point(173, 150);
            this.txtIncomesID.Multiline = true;
            this.txtIncomesID.Name = "txtIncomesID";
            this.txtIncomesID.Size = new System.Drawing.Size(258, 22);
            this.txtIncomesID.TabIndex = 50;
            this.txtIncomesID.Validating += new System.ComponentModel.CancelEventHandler(this.txtIncomesID_Validating);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(21, 150);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(73, 17);
            this.label26.TabIndex = 37;
            this.label26.Text = "IncomesID";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(21, 190);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(85, 17);
            this.label28.TabIndex = 38;
            this.label28.Text = "IncomeType";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(21, 224);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(98, 17);
            this.label30.TabIndex = 39;
            this.label30.Text = "Income Owner";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(21, 325);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(146, 17);
            this.label33.TabIndex = 43;
            this.label33.Text = "Part Time Percentage";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(21, 253);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(72, 17);
            this.label34.TabIndex = 41;
            this.label34.Text = "Start Date";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(21, 291);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(67, 17);
            this.label35.TabIndex = 42;
            this.label35.Text = "End Date";
            // 
            // tabPensions
            // 
            this.tabPensions.Controls.Add(this.gbPensions);
            this.tabPensions.Location = new System.Drawing.Point(4, 25);
            this.tabPensions.Name = "tabPensions";
            this.tabPensions.Padding = new System.Windows.Forms.Padding(3);
            this.tabPensions.Size = new System.Drawing.Size(1225, 574);
            this.tabPensions.TabIndex = 2;
            this.tabPensions.Text = "Pensions";
            this.tabPensions.UseVisualStyleBackColor = true;
            // 
            // gbPensions
            // 
            this.gbPensions.BackColor = System.Drawing.Color.Silver;
            this.gbPensions.Controls.Add(this.cbNoPensionData);
            this.gbPensions.Controls.Add(this.dpPensionStartDate);
            this.gbPensions.Controls.Add(this.cbPensionType);
            this.gbPensions.Controls.Add(this.txtPensionValue);
            this.gbPensions.Controls.Add(this.txtPensionOwner);
            this.gbPensions.Controls.Add(this.txtPensionID);
            this.gbPensions.Controls.Add(this.label38);
            this.gbPensions.Controls.Add(this.label39);
            this.gbPensions.Controls.Add(this.label40);
            this.gbPensions.Controls.Add(this.label41);
            this.gbPensions.Controls.Add(this.label42);
            this.gbPensions.Location = new System.Drawing.Point(25, 12);
            this.gbPensions.Name = "gbPensions";
            this.gbPensions.Size = new System.Drawing.Size(1194, 550);
            this.gbPensions.TabIndex = 1;
            this.gbPensions.TabStop = false;
            this.gbPensions.Text = "Pensions";
            // 
            // cbNoPensionData
            // 
            this.cbNoPensionData.AutoSize = true;
            this.cbNoPensionData.Location = new System.Drawing.Point(231, 43);
            this.cbNoPensionData.Name = "cbNoPensionData";
            this.cbNoPensionData.Size = new System.Drawing.Size(137, 21);
            this.cbNoPensionData.TabIndex = 63;
            this.cbNoPensionData.Text = "No Pension Data";
            this.cbNoPensionData.UseVisualStyleBackColor = true;
            this.cbNoPensionData.CheckStateChanged += new System.EventHandler(this.cbNoPensionData_CheckStateChanged);
            // 
            // dpPensionStartDate
            // 
            this.dpPensionStartDate.Location = new System.Drawing.Point(380, 240);
            this.dpPensionStartDate.Name = "dpPensionStartDate";
            this.dpPensionStartDate.Size = new System.Drawing.Size(296, 22);
            this.dpPensionStartDate.TabIndex = 61;
            // 
            // cbPensionType
            // 
            this.cbPensionType.FormattingEnabled = true;
            this.cbPensionType.Location = new System.Drawing.Point(380, 170);
            this.cbPensionType.Name = "cbPensionType";
            this.cbPensionType.Size = new System.Drawing.Size(296, 24);
            this.cbPensionType.TabIndex = 60;
            // 
            // txtPensionValue
            // 
            this.txtPensionValue.Location = new System.Drawing.Point(380, 273);
            this.txtPensionValue.Multiline = true;
            this.txtPensionValue.Name = "txtPensionValue";
            this.txtPensionValue.Size = new System.Drawing.Size(296, 22);
            this.txtPensionValue.TabIndex = 56;
            // 
            // txtPensionOwner
            // 
            this.txtPensionOwner.Location = new System.Drawing.Point(380, 208);
            this.txtPensionOwner.Multiline = true;
            this.txtPensionOwner.Name = "txtPensionOwner";
            this.txtPensionOwner.Size = new System.Drawing.Size(296, 22);
            this.txtPensionOwner.TabIndex = 54;
            // 
            // txtPensionID
            // 
            this.txtPensionID.Location = new System.Drawing.Point(380, 134);
            this.txtPensionID.Multiline = true;
            this.txtPensionID.Name = "txtPensionID";
            this.txtPensionID.Size = new System.Drawing.Size(296, 22);
            this.txtPensionID.TabIndex = 50;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(228, 134);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(72, 17);
            this.label38.TabIndex = 37;
            this.label38.Text = "PensionID";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(228, 174);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(95, 17);
            this.label39.TabIndex = 38;
            this.label39.Text = "Pension Type";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(228, 208);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(104, 17);
            this.label40.TabIndex = 39;
            this.label40.Text = "Pension Owner";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(228, 237);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(72, 17);
            this.label41.TabIndex = 40;
            this.label41.Text = "Start Date";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(228, 273);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(44, 17);
            this.label42.TabIndex = 43;
            this.label42.Text = "Value";
            // 
            // tabExpenses
            // 
            this.tabExpenses.Controls.Add(this.gbExpenses);
            this.tabExpenses.Location = new System.Drawing.Point(4, 25);
            this.tabExpenses.Name = "tabExpenses";
            this.tabExpenses.Padding = new System.Windows.Forms.Padding(3);
            this.tabExpenses.Size = new System.Drawing.Size(1225, 574);
            this.tabExpenses.TabIndex = 3;
            this.tabExpenses.Text = "Expenses";
            this.tabExpenses.UseVisualStyleBackColor = true;
            // 
            // gbExpenses
            // 
            this.gbExpenses.BackColor = System.Drawing.Color.Silver;
            this.gbExpenses.Controls.Add(this.cbNoExpensesData);
            this.gbExpenses.Controls.Add(this.cbExpenseType);
            this.gbExpenses.Controls.Add(this.txtExpenseMade);
            this.gbExpenses.Controls.Add(this.txtExpenseOwner);
            this.gbExpenses.Controls.Add(this.txtExpenseID);
            this.gbExpenses.Controls.Add(this.label43);
            this.gbExpenses.Controls.Add(this.label44);
            this.gbExpenses.Controls.Add(this.label45);
            this.gbExpenses.Controls.Add(this.label47);
            this.gbExpenses.Location = new System.Drawing.Point(15, 12);
            this.gbExpenses.Name = "gbExpenses";
            this.gbExpenses.Size = new System.Drawing.Size(1194, 550);
            this.gbExpenses.TabIndex = 2;
            this.gbExpenses.TabStop = false;
            this.gbExpenses.Text = "Expenses";
            // 
            // cbNoExpensesData
            // 
            this.cbNoExpensesData.AutoSize = true;
            this.cbNoExpensesData.Location = new System.Drawing.Point(229, 62);
            this.cbNoExpensesData.Name = "cbNoExpensesData";
            this.cbNoExpensesData.Size = new System.Drawing.Size(147, 21);
            this.cbNoExpensesData.TabIndex = 64;
            this.cbNoExpensesData.Text = "No Expenses Data";
            this.cbNoExpensesData.UseVisualStyleBackColor = true;
            this.cbNoExpensesData.CheckStateChanged += new System.EventHandler(this.cbNoExpensesData_CheckStateChanged);
            // 
            // cbExpenseType
            // 
            this.cbExpenseType.FormattingEnabled = true;
            this.cbExpenseType.Location = new System.Drawing.Point(378, 171);
            this.cbExpenseType.Name = "cbExpenseType";
            this.cbExpenseType.Size = new System.Drawing.Size(285, 24);
            this.cbExpenseType.TabIndex = 60;
            // 
            // txtExpenseMade
            // 
            this.txtExpenseMade.Location = new System.Drawing.Point(378, 246);
            this.txtExpenseMade.Multiline = true;
            this.txtExpenseMade.Name = "txtExpenseMade";
            this.txtExpenseMade.Size = new System.Drawing.Size(285, 22);
            this.txtExpenseMade.TabIndex = 56;
            // 
            // txtExpenseOwner
            // 
            this.txtExpenseOwner.Location = new System.Drawing.Point(378, 207);
            this.txtExpenseOwner.Multiline = true;
            this.txtExpenseOwner.Name = "txtExpenseOwner";
            this.txtExpenseOwner.Size = new System.Drawing.Size(285, 22);
            this.txtExpenseOwner.TabIndex = 54;
            // 
            // txtExpenseID
            // 
            this.txtExpenseID.Location = new System.Drawing.Point(378, 135);
            this.txtExpenseID.Multiline = true;
            this.txtExpenseID.Name = "txtExpenseID";
            this.txtExpenseID.Size = new System.Drawing.Size(285, 22);
            this.txtExpenseID.TabIndex = 50;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(226, 135);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(75, 17);
            this.label43.TabIndex = 37;
            this.label43.Text = "ExpenseID";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(226, 175);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(98, 17);
            this.label44.TabIndex = 38;
            this.label44.Text = "Expense Type";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(226, 209);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(107, 17);
            this.label45.TabIndex = 39;
            this.label45.Text = "Expense Owner";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(226, 246);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(101, 17);
            this.label47.TabIndex = 43;
            this.label47.Text = "Expense Made";
            // 
            // tabAssets
            // 
            this.tabAssets.Controls.Add(this.gbAssets);
            this.tabAssets.Location = new System.Drawing.Point(4, 25);
            this.tabAssets.Name = "tabAssets";
            this.tabAssets.Size = new System.Drawing.Size(1225, 574);
            this.tabAssets.TabIndex = 4;
            this.tabAssets.Text = "Assets";
            this.tabAssets.UseVisualStyleBackColor = true;
            this.tabAssets.Visible = false;
            // 
            // gbAssets
            // 
            this.gbAssets.BackColor = System.Drawing.Color.Silver;
            this.gbAssets.Controls.Add(this.cbNoAssetsData);
            this.gbAssets.Controls.Add(this.txtAssetInterestRate);
            this.gbAssets.Controls.Add(this.label51);
            this.gbAssets.Controls.Add(this.cbAssetType);
            this.gbAssets.Controls.Add(this.txtAssetValue);
            this.gbAssets.Controls.Add(this.txtAssetOwner);
            this.gbAssets.Controls.Add(this.txtAssetID);
            this.gbAssets.Controls.Add(this.label46);
            this.gbAssets.Controls.Add(this.label48);
            this.gbAssets.Controls.Add(this.label49);
            this.gbAssets.Controls.Add(this.label50);
            this.gbAssets.Location = new System.Drawing.Point(15, 12);
            this.gbAssets.Name = "gbAssets";
            this.gbAssets.Size = new System.Drawing.Size(1194, 550);
            this.gbAssets.TabIndex = 3;
            this.gbAssets.TabStop = false;
            this.gbAssets.Text = "Assets";
            // 
            // cbNoAssetsData
            // 
            this.cbNoAssetsData.AutoSize = true;
            this.cbNoAssetsData.Location = new System.Drawing.Point(226, 42);
            this.cbNoAssetsData.Name = "cbNoAssetsData";
            this.cbNoAssetsData.Size = new System.Drawing.Size(128, 21);
            this.cbNoAssetsData.TabIndex = 69;
            this.cbNoAssetsData.Text = "No Assets Data";
            this.cbNoAssetsData.UseVisualStyleBackColor = true;
            this.cbNoAssetsData.CheckStateChanged += new System.EventHandler(this.cbNoAssetsData_CheckStateChanged);
            // 
            // txtAssetInterestRate
            // 
            this.txtAssetInterestRate.Location = new System.Drawing.Point(375, 280);
            this.txtAssetInterestRate.Multiline = true;
            this.txtAssetInterestRate.Name = "txtAssetInterestRate";
            this.txtAssetInterestRate.Size = new System.Drawing.Size(288, 22);
            this.txtAssetInterestRate.TabIndex = 67;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(223, 280);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(128, 17);
            this.label51.TabIndex = 66;
            this.label51.Text = "Asset Interest Rate";
            // 
            // cbAssetType
            // 
            this.cbAssetType.FormattingEnabled = true;
            this.cbAssetType.Location = new System.Drawing.Point(375, 170);
            this.cbAssetType.Name = "cbAssetType";
            this.cbAssetType.Size = new System.Drawing.Size(288, 24);
            this.cbAssetType.TabIndex = 60;
            // 
            // txtAssetValue
            // 
            this.txtAssetValue.Location = new System.Drawing.Point(375, 245);
            this.txtAssetValue.Multiline = true;
            this.txtAssetValue.Name = "txtAssetValue";
            this.txtAssetValue.Size = new System.Drawing.Size(288, 22);
            this.txtAssetValue.TabIndex = 56;
            // 
            // txtAssetOwner
            // 
            this.txtAssetOwner.Location = new System.Drawing.Point(375, 207);
            this.txtAssetOwner.Multiline = true;
            this.txtAssetOwner.Name = "txtAssetOwner";
            this.txtAssetOwner.Size = new System.Drawing.Size(288, 22);
            this.txtAssetOwner.TabIndex = 54;
            // 
            // txtAssetID
            // 
            this.txtAssetID.Location = new System.Drawing.Point(375, 134);
            this.txtAssetID.Multiline = true;
            this.txtAssetID.Name = "txtAssetID";
            this.txtAssetID.Size = new System.Drawing.Size(288, 22);
            this.txtAssetID.TabIndex = 50;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(223, 134);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(56, 17);
            this.label46.TabIndex = 37;
            this.label46.Text = "AssetID";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(223, 174);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(79, 17);
            this.label48.TabIndex = 38;
            this.label48.Text = "Asset Type";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(223, 208);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(88, 17);
            this.label49.TabIndex = 39;
            this.label49.Text = "Asset Owner";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(223, 245);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(83, 17);
            this.label50.TabIndex = 43;
            this.label50.Text = "Asset Value";
            // 
            // tabLoans
            // 
            this.tabLoans.Controls.Add(this.gbLoans);
            this.tabLoans.Location = new System.Drawing.Point(4, 25);
            this.tabLoans.Name = "tabLoans";
            this.tabLoans.Size = new System.Drawing.Size(1225, 574);
            this.tabLoans.TabIndex = 5;
            this.tabLoans.Text = "Loans";
            this.tabLoans.UseVisualStyleBackColor = true;
            this.tabLoans.Visible = false;
            // 
            // gbLoans
            // 
            this.gbLoans.BackColor = System.Drawing.Color.Silver;
            this.gbLoans.Controls.Add(this.cbNoLoansData);
            this.gbLoans.Controls.Add(this.cbLoanType);
            this.gbLoans.Controls.Add(this.txtLoanAmount);
            this.gbLoans.Controls.Add(this.txtLoanOwner);
            this.gbLoans.Controls.Add(this.txtLoanID);
            this.gbLoans.Controls.Add(this.label53);
            this.gbLoans.Controls.Add(this.label54);
            this.gbLoans.Controls.Add(this.label55);
            this.gbLoans.Controls.Add(this.label56);
            this.gbLoans.Location = new System.Drawing.Point(15, 12);
            this.gbLoans.Name = "gbLoans";
            this.gbLoans.Size = new System.Drawing.Size(1194, 550);
            this.gbLoans.TabIndex = 4;
            this.gbLoans.TabStop = false;
            this.gbLoans.Text = "Loans";
            // 
            // cbNoLoansData
            // 
            this.cbNoLoansData.AutoSize = true;
            this.cbNoLoansData.Location = new System.Drawing.Point(220, 65);
            this.cbNoLoansData.Name = "cbNoLoansData";
            this.cbNoLoansData.Size = new System.Drawing.Size(125, 21);
            this.cbNoLoansData.TabIndex = 64;
            this.cbNoLoansData.Text = "No Loans Data";
            this.cbNoLoansData.UseVisualStyleBackColor = true;
            this.cbNoLoansData.CheckStateChanged += new System.EventHandler(this.cbNoLoansData_CheckStateChanged);
            // 
            // cbLoanType
            // 
            this.cbLoanType.FormattingEnabled = true;
            this.cbLoanType.Location = new System.Drawing.Point(369, 203);
            this.cbLoanType.Name = "cbLoanType";
            this.cbLoanType.Size = new System.Drawing.Size(307, 24);
            this.cbLoanType.TabIndex = 60;
            // 
            // txtLoanAmount
            // 
            this.txtLoanAmount.Location = new System.Drawing.Point(369, 278);
            this.txtLoanAmount.Multiline = true;
            this.txtLoanAmount.Name = "txtLoanAmount";
            this.txtLoanAmount.Size = new System.Drawing.Size(307, 22);
            this.txtLoanAmount.TabIndex = 56;
            // 
            // txtLoanOwner
            // 
            this.txtLoanOwner.Location = new System.Drawing.Point(369, 240);
            this.txtLoanOwner.Multiline = true;
            this.txtLoanOwner.Name = "txtLoanOwner";
            this.txtLoanOwner.Size = new System.Drawing.Size(307, 22);
            this.txtLoanOwner.TabIndex = 54;
            // 
            // txtLoanID
            // 
            this.txtLoanID.Location = new System.Drawing.Point(369, 167);
            this.txtLoanID.Multiline = true;
            this.txtLoanID.Name = "txtLoanID";
            this.txtLoanID.Size = new System.Drawing.Size(307, 22);
            this.txtLoanID.TabIndex = 50;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(217, 167);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(53, 17);
            this.label53.TabIndex = 37;
            this.label53.Text = "LoanID";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(217, 207);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(76, 17);
            this.label54.TabIndex = 38;
            this.label54.Text = "Loan Type";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(217, 241);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(85, 17);
            this.label55.TabIndex = 39;
            this.label55.Text = "Loan Owner";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(217, 278);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(92, 17);
            this.label56.TabIndex = 43;
            this.label56.Text = "Loan Amount";
            // 
            // tabRealProperty
            // 
            this.tabRealProperty.Controls.Add(this.groupBox4);
            this.tabRealProperty.Controls.Add(this.gbRealPropertyHouse);
            this.tabRealProperty.Location = new System.Drawing.Point(4, 25);
            this.tabRealProperty.Name = "tabRealProperty";
            this.tabRealProperty.Size = new System.Drawing.Size(1225, 574);
            this.tabRealProperty.TabIndex = 6;
            this.tabRealProperty.Text = "Real Property";
            this.tabRealProperty.UseVisualStyleBackColor = true;
            this.tabRealProperty.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Silver;
            this.groupBox4.Controls.Add(this.cbNoLeaseHoldData);
            this.groupBox4.Controls.Add(this.cbLeaseType);
            this.groupBox4.Controls.Add(this.dpLeaseHoldEndDate);
            this.groupBox4.Controls.Add(this.txtLeaseHoldID);
            this.groupBox4.Controls.Add(this.txtLeaseHoldValue);
            this.groupBox4.Controls.Add(this.label65);
            this.groupBox4.Controls.Add(this.label66);
            this.groupBox4.Controls.Add(this.label67);
            this.groupBox4.Controls.Add(this.label71);
            this.groupBox4.Location = new System.Drawing.Point(815, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(397, 562);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Leasehold";
            // 
            // cbNoLeaseHoldData
            // 
            this.cbNoLeaseHoldData.AutoSize = true;
            this.cbNoLeaseHoldData.Location = new System.Drawing.Point(35, 27);
            this.cbNoLeaseHoldData.Name = "cbNoLeaseHoldData";
            this.cbNoLeaseHoldData.Size = new System.Drawing.Size(152, 21);
            this.cbNoLeaseHoldData.TabIndex = 67;
            this.cbNoLeaseHoldData.Text = "No Leasehold Data";
            this.cbNoLeaseHoldData.UseVisualStyleBackColor = true;
            this.cbNoLeaseHoldData.CheckStateChanged += new System.EventHandler(this.cbNoLeaseHoldData_CheckStateChanged);
            // 
            // cbLeaseType
            // 
            this.cbLeaseType.FormattingEnabled = true;
            this.cbLeaseType.Location = new System.Drawing.Point(139, 87);
            this.cbLeaseType.Name = "cbLeaseType";
            this.cbLeaseType.Size = new System.Drawing.Size(229, 24);
            this.cbLeaseType.TabIndex = 53;
            // 
            // dpLeaseHoldEndDate
            // 
            this.dpLeaseHoldEndDate.Location = new System.Drawing.Point(139, 159);
            this.dpLeaseHoldEndDate.Name = "dpLeaseHoldEndDate";
            this.dpLeaseHoldEndDate.Size = new System.Drawing.Size(229, 22);
            this.dpLeaseHoldEndDate.TabIndex = 52;
            // 
            // txtLeaseHoldID
            // 
            this.txtLeaseHoldID.Location = new System.Drawing.Point(139, 54);
            this.txtLeaseHoldID.Multiline = true;
            this.txtLeaseHoldID.Name = "txtLeaseHoldID";
            this.txtLeaseHoldID.Size = new System.Drawing.Size(229, 22);
            this.txtLeaseHoldID.TabIndex = 40;
            // 
            // txtLeaseHoldValue
            // 
            this.txtLeaseHoldValue.Location = new System.Drawing.Point(139, 119);
            this.txtLeaseHoldValue.Multiline = true;
            this.txtLeaseHoldValue.Name = "txtLeaseHoldValue";
            this.txtLeaseHoldValue.Size = new System.Drawing.Size(229, 22);
            this.txtLeaseHoldValue.TabIndex = 38;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(32, 162);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(67, 17);
            this.label65.TabIndex = 35;
            this.label65.Text = "End Date";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(32, 54);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(89, 17);
            this.label66.TabIndex = 34;
            this.label66.Text = "LeaseHoldID";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(32, 87);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(40, 17);
            this.label67.TabIndex = 33;
            this.label67.Text = "Type";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(32, 120);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(44, 17);
            this.label71.TabIndex = 32;
            this.label71.Text = "Value";
            // 
            // gbRealPropertyHouse
            // 
            this.gbRealPropertyHouse.BackColor = System.Drawing.Color.Silver;
            this.gbRealPropertyHouse.Controls.Add(this.cbNoHouseData);
            this.gbRealPropertyHouse.Controls.Add(this.cbHouseObjectType);
            this.gbRealPropertyHouse.Controls.Add(this.label94);
            this.gbRealPropertyHouse.Controls.Add(this.label73);
            this.gbRealPropertyHouse.Controls.Add(this.txtHouseStreetName);
            this.gbRealPropertyHouse.Controls.Add(this.txtHouseExtraWorkContractor);
            this.gbRealPropertyHouse.Controls.Add(this.txtHouseHouseNumber);
            this.gbRealPropertyHouse.Controls.Add(this.txtHousePostalCode);
            this.gbRealPropertyHouse.Controls.Add(this.dpHousePurchaseDate);
            this.gbRealPropertyHouse.Controls.Add(this.txtHouseCity);
            this.gbRealPropertyHouse.Controls.Add(this.txtContractorSum);
            this.gbRealPropertyHouse.Controls.Add(this.txtHouseMunicipality);
            this.gbRealPropertyHouse.Controls.Add(this.txtHouseOverdueMaintenance);
            this.gbRealPropertyHouse.Controls.Add(this.label52);
            this.gbRealPropertyHouse.Controls.Add(this.txtHouseEconomicValue);
            this.gbRealPropertyHouse.Controls.Add(this.label57);
            this.gbRealPropertyHouse.Controls.Add(this.label58);
            this.gbRealPropertyHouse.Controls.Add(this.txtHouseEcoValueAfterImprovements);
            this.gbRealPropertyHouse.Controls.Add(this.label59);
            this.gbRealPropertyHouse.Controls.Add(this.txtHouseGroundPrice);
            this.gbRealPropertyHouse.Controls.Add(this.label60);
            this.gbRealPropertyHouse.Controls.Add(this.label61);
            this.gbRealPropertyHouse.Controls.Add(this.label62);
            this.gbRealPropertyHouse.Controls.Add(this.label63);
            this.gbRealPropertyHouse.Controls.Add(this.label64);
            this.gbRealPropertyHouse.Controls.Add(this.label78);
            this.gbRealPropertyHouse.Controls.Add(this.label79);
            this.gbRealPropertyHouse.Controls.Add(this.cbHouseType);
            this.gbRealPropertyHouse.Controls.Add(this.txtHouseProjectName);
            this.gbRealPropertyHouse.Controls.Add(this.txtHouseProjectNumber);
            this.gbRealPropertyHouse.Controls.Add(this.txtHouseID);
            this.gbRealPropertyHouse.Controls.Add(this.label68);
            this.gbRealPropertyHouse.Controls.Add(this.label70);
            this.gbRealPropertyHouse.Controls.Add(this.label72);
            this.gbRealPropertyHouse.Controls.Add(this.label76);
            this.gbRealPropertyHouse.Controls.Add(this.label77);
            this.gbRealPropertyHouse.Location = new System.Drawing.Point(12, 7);
            this.gbRealPropertyHouse.Name = "gbRealPropertyHouse";
            this.gbRealPropertyHouse.Size = new System.Drawing.Size(797, 562);
            this.gbRealPropertyHouse.TabIndex = 20;
            this.gbRealPropertyHouse.TabStop = false;
            this.gbRealPropertyHouse.Text = "House";
            this.gbRealPropertyHouse.Enter += new System.EventHandler(this.gbRealPropertyHouse_Enter);
            // 
            // cbNoHouseData
            // 
            this.cbNoHouseData.AutoSize = true;
            this.cbNoHouseData.Location = new System.Drawing.Point(21, 21);
            this.cbNoHouseData.Name = "cbNoHouseData";
            this.cbNoHouseData.Size = new System.Drawing.Size(127, 21);
            this.cbNoHouseData.TabIndex = 67;
            this.cbNoHouseData.TabStop = false;
            this.cbNoHouseData.Text = "No House Data";
            this.cbNoHouseData.UseVisualStyleBackColor = true;
            this.cbNoHouseData.CheckStateChanged += new System.EventHandler(this.cbNoHouseData_CheckStateChanged);
            // 
            // cbHouseObjectType
            // 
            this.cbHouseObjectType.FormattingEnabled = true;
            this.cbHouseObjectType.Location = new System.Drawing.Point(170, 130);
            this.cbHouseObjectType.Name = "cbHouseObjectType";
            this.cbHouseObjectType.Size = new System.Drawing.Size(144, 24);
            this.cbHouseObjectType.TabIndex = 56;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(18, 134);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(85, 17);
            this.label94.TabIndex = 55;
            this.label94.Text = "Object Type";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(437, 155);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(31, 17);
            this.label73.TabIndex = 54;
            this.label73.Text = "City";
            // 
            // txtHouseStreetName
            // 
            this.txtHouseStreetName.Location = new System.Drawing.Point(544, 54);
            this.txtHouseStreetName.Multiline = true;
            this.txtHouseStreetName.Name = "txtHouseStreetName";
            this.txtHouseStreetName.Size = new System.Drawing.Size(229, 22);
            this.txtHouseStreetName.TabIndex = 30;
            // 
            // txtHouseExtraWorkContractor
            // 
            this.txtHouseExtraWorkContractor.Location = new System.Drawing.Point(171, 322);
            this.txtHouseExtraWorkContractor.Multiline = true;
            this.txtHouseExtraWorkContractor.Name = "txtHouseExtraWorkContractor";
            this.txtHouseExtraWorkContractor.Size = new System.Drawing.Size(144, 22);
            this.txtHouseExtraWorkContractor.TabIndex = 53;
            // 
            // txtHouseHouseNumber
            // 
            this.txtHouseHouseNumber.Location = new System.Drawing.Point(544, 87);
            this.txtHouseHouseNumber.Multiline = true;
            this.txtHouseHouseNumber.Name = "txtHouseHouseNumber";
            this.txtHouseHouseNumber.Size = new System.Drawing.Size(229, 22);
            this.txtHouseHouseNumber.TabIndex = 29;
            // 
            // txtHousePostalCode
            // 
            this.txtHousePostalCode.Location = new System.Drawing.Point(544, 119);
            this.txtHousePostalCode.Multiline = true;
            this.txtHousePostalCode.Name = "txtHousePostalCode";
            this.txtHousePostalCode.Size = new System.Drawing.Size(229, 22);
            this.txtHousePostalCode.TabIndex = 28;
            // 
            // dpHousePurchaseDate
            // 
            this.dpHousePurchaseDate.Location = new System.Drawing.Point(170, 172);
            this.dpHousePurchaseDate.Name = "dpHousePurchaseDate";
            this.dpHousePurchaseDate.Size = new System.Drawing.Size(144, 22);
            this.dpHousePurchaseDate.TabIndex = 51;
            // 
            // txtHouseCity
            // 
            this.txtHouseCity.Location = new System.Drawing.Point(544, 150);
            this.txtHouseCity.Multiline = true;
            this.txtHouseCity.Name = "txtHouseCity";
            this.txtHouseCity.Size = new System.Drawing.Size(229, 22);
            this.txtHouseCity.TabIndex = 27;
            // 
            // txtContractorSum
            // 
            this.txtContractorSum.Location = new System.Drawing.Point(171, 358);
            this.txtContractorSum.Multiline = true;
            this.txtContractorSum.Name = "txtContractorSum";
            this.txtContractorSum.Size = new System.Drawing.Size(144, 22);
            this.txtContractorSum.TabIndex = 48;
            // 
            // txtHouseMunicipality
            // 
            this.txtHouseMunicipality.Location = new System.Drawing.Point(544, 186);
            this.txtHouseMunicipality.Multiline = true;
            this.txtHouseMunicipality.Name = "txtHouseMunicipality";
            this.txtHouseMunicipality.Size = new System.Drawing.Size(229, 22);
            this.txtHouseMunicipality.TabIndex = 26;
            // 
            // txtHouseOverdueMaintenance
            // 
            this.txtHouseOverdueMaintenance.Location = new System.Drawing.Point(171, 390);
            this.txtHouseOverdueMaintenance.Multiline = true;
            this.txtHouseOverdueMaintenance.Name = "txtHouseOverdueMaintenance";
            this.txtHouseOverdueMaintenance.Size = new System.Drawing.Size(144, 22);
            this.txtHouseOverdueMaintenance.TabIndex = 47;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(437, 194);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(81, 17);
            this.label52.TabIndex = 25;
            this.label52.Text = "Municipality";
            // 
            // txtHouseEconomicValue
            // 
            this.txtHouseEconomicValue.Location = new System.Drawing.Point(171, 422);
            this.txtHouseEconomicValue.Multiline = true;
            this.txtHouseEconomicValue.Name = "txtHouseEconomicValue";
            this.txtHouseEconomicValue.Size = new System.Drawing.Size(144, 22);
            this.txtHouseEconomicValue.TabIndex = 46;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(437, 54);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(87, 17);
            this.label57.TabIndex = 24;
            this.label57.Text = "Street Name";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(437, 87);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(103, 17);
            this.label58.TabIndex = 23;
            this.label58.Text = "House Number";
            // 
            // txtHouseEcoValueAfterImprovements
            // 
            this.txtHouseEcoValueAfterImprovements.Location = new System.Drawing.Point(170, 491);
            this.txtHouseEcoValueAfterImprovements.Multiline = true;
            this.txtHouseEcoValueAfterImprovements.Name = "txtHouseEcoValueAfterImprovements";
            this.txtHouseEcoValueAfterImprovements.Size = new System.Drawing.Size(144, 22);
            this.txtHouseEcoValueAfterImprovements.TabIndex = 45;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(460, 196);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(31, 17);
            this.label59.TabIndex = 21;
            this.label59.Text = "City";
            // 
            // txtHouseGroundPrice
            // 
            this.txtHouseGroundPrice.Location = new System.Drawing.Point(171, 284);
            this.txtHouseGroundPrice.Multiline = true;
            this.txtHouseGroundPrice.Name = "txtHouseGroundPrice";
            this.txtHouseGroundPrice.Size = new System.Drawing.Size(144, 22);
            this.txtHouseGroundPrice.TabIndex = 44;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(437, 119);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(84, 17);
            this.label60.TabIndex = 22;
            this.label60.Text = "Postal Code";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(19, 284);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(92, 17);
            this.label61.TabIndex = 37;
            this.label61.Text = "Ground Price";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(19, 324);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(147, 17);
            this.label62.TabIndex = 38;
            this.label62.Text = "Extra Work Contractor";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(19, 358);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(106, 17);
            this.label63.TabIndex = 39;
            this.label63.Text = "Contractor Sum";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(19, 387);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(148, 17);
            this.label64.TabIndex = 40;
            this.label64.Text = "Overdue Maintenance";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(19, 419);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(109, 17);
            this.label78.TabIndex = 41;
            this.label78.Text = "Economic Value";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(19, 457);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(235, 17);
            this.label79.TabIndex = 42;
            this.label79.Text = "Economic Value After Improvements";
            // 
            // cbHouseType
            // 
            this.cbHouseType.FormattingEnabled = true;
            this.cbHouseType.Location = new System.Drawing.Point(170, 89);
            this.cbHouseType.Name = "cbHouseType";
            this.cbHouseType.Size = new System.Drawing.Size(144, 24);
            this.cbHouseType.TabIndex = 33;
            // 
            // txtHouseProjectName
            // 
            this.txtHouseProjectName.Location = new System.Drawing.Point(170, 211);
            this.txtHouseProjectName.Multiline = true;
            this.txtHouseProjectName.Name = "txtHouseProjectName";
            this.txtHouseProjectName.Size = new System.Drawing.Size(144, 22);
            this.txtHouseProjectName.TabIndex = 19;
            // 
            // txtHouseProjectNumber
            // 
            this.txtHouseProjectNumber.Location = new System.Drawing.Point(170, 244);
            this.txtHouseProjectNumber.Multiline = true;
            this.txtHouseProjectNumber.Name = "txtHouseProjectNumber";
            this.txtHouseProjectNumber.Size = new System.Drawing.Size(144, 22);
            this.txtHouseProjectNumber.TabIndex = 18;
            // 
            // txtHouseID
            // 
            this.txtHouseID.Location = new System.Drawing.Point(170, 53);
            this.txtHouseID.Multiline = true;
            this.txtHouseID.Name = "txtHouseID";
            this.txtHouseID.Size = new System.Drawing.Size(144, 22);
            this.txtHouseID.TabIndex = 17;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(18, 53);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(62, 17);
            this.label68.TabIndex = 0;
            this.label68.Text = "HouseID";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(18, 93);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(85, 17);
            this.label70.TabIndex = 1;
            this.label70.Text = "House Type";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(18, 174);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(102, 17);
            this.label72.TabIndex = 2;
            this.label72.Text = "Purchase Date";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(18, 208);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(93, 17);
            this.label76.TabIndex = 4;
            this.label76.Text = "Project Name";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(18, 243);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(106, 17);
            this.label77.TabIndex = 5;
            this.label77.Text = "Project Number";
            // 
            // tabFinanceConstruction
            // 
            this.tabFinanceConstruction.Controls.Add(this.groupBox3);
            this.tabFinanceConstruction.Location = new System.Drawing.Point(4, 25);
            this.tabFinanceConstruction.Name = "tabFinanceConstruction";
            this.tabFinanceConstruction.Size = new System.Drawing.Size(1225, 574);
            this.tabFinanceConstruction.TabIndex = 8;
            this.tabFinanceConstruction.Text = "Finance Construction";
            this.tabFinanceConstruction.UseVisualStyleBackColor = true;
            this.tabFinanceConstruction.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Silver;
            this.groupBox3.Controls.Add(this.cbNoFinanceConstructionData);
            this.groupBox3.Controls.Add(this.dpMortgageStartDate);
            this.groupBox3.Controls.Add(this.txtMortgageTotal);
            this.groupBox3.Controls.Add(this.label83);
            this.groupBox3.Controls.Add(this.txtMortgageRegistration);
            this.groupBox3.Controls.Add(this.label69);
            this.groupBox3.Controls.Add(this.txtMortGageDeed);
            this.groupBox3.Controls.Add(this.txtAdviceFee);
            this.groupBox3.Controls.Add(this.txtMarketValueReport);
            this.groupBox3.Controls.Add(this.label75);
            this.groupBox3.Controls.Add(this.label80);
            this.groupBox3.Controls.Add(this.label81);
            this.groupBox3.Controls.Add(this.label82);
            this.groupBox3.Location = new System.Drawing.Point(15, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1194, 550);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Assets";
            // 
            // cbNoFinanceConstructionData
            // 
            this.cbNoFinanceConstructionData.AutoSize = true;
            this.cbNoFinanceConstructionData.Location = new System.Drawing.Point(333, 49);
            this.cbNoFinanceConstructionData.Name = "cbNoFinanceConstructionData";
            this.cbNoFinanceConstructionData.Size = new System.Drawing.Size(219, 21);
            this.cbNoFinanceConstructionData.TabIndex = 73;
            this.cbNoFinanceConstructionData.Text = "No Finance Construction Data";
            this.cbNoFinanceConstructionData.UseVisualStyleBackColor = true;
            this.cbNoFinanceConstructionData.CheckStateChanged += new System.EventHandler(this.cbNoFinanceConstructionData_CheckStateChanged);
            // 
            // dpMortgageStartDate
            // 
            this.dpMortgageStartDate.Location = new System.Drawing.Point(482, 279);
            this.dpMortgageStartDate.Name = "dpMortgageStartDate";
            this.dpMortgageStartDate.Size = new System.Drawing.Size(181, 22);
            this.dpMortgageStartDate.TabIndex = 71;
            // 
            // txtMortgageTotal
            // 
            this.txtMortgageTotal.Location = new System.Drawing.Point(482, 307);
            this.txtMortgageTotal.Multiline = true;
            this.txtMortgageTotal.Name = "txtMortgageTotal";
            this.txtMortgageTotal.Size = new System.Drawing.Size(181, 22);
            this.txtMortgageTotal.TabIndex = 70;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(330, 307);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(104, 17);
            this.label83.TabIndex = 69;
            this.label83.Text = "Mortgage Total";
            // 
            // txtMortgageRegistration
            // 
            this.txtMortgageRegistration.Location = new System.Drawing.Point(482, 170);
            this.txtMortgageRegistration.Multiline = true;
            this.txtMortgageRegistration.Name = "txtMortgageRegistration";
            this.txtMortgageRegistration.Size = new System.Drawing.Size(181, 22);
            this.txtMortgageRegistration.TabIndex = 68;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(330, 279);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(72, 17);
            this.label69.TabIndex = 66;
            this.label69.Text = "Start Date";
            // 
            // txtMortGageDeed
            // 
            this.txtMortGageDeed.Location = new System.Drawing.Point(482, 244);
            this.txtMortGageDeed.Multiline = true;
            this.txtMortGageDeed.Name = "txtMortGageDeed";
            this.txtMortGageDeed.Size = new System.Drawing.Size(181, 22);
            this.txtMortGageDeed.TabIndex = 56;
            // 
            // txtAdviceFee
            // 
            this.txtAdviceFee.Location = new System.Drawing.Point(482, 207);
            this.txtAdviceFee.Multiline = true;
            this.txtAdviceFee.Name = "txtAdviceFee";
            this.txtAdviceFee.Size = new System.Drawing.Size(181, 22);
            this.txtAdviceFee.TabIndex = 54;
            // 
            // txtMarketValueReport
            // 
            this.txtMarketValueReport.Location = new System.Drawing.Point(482, 133);
            this.txtMarketValueReport.Multiline = true;
            this.txtMarketValueReport.Name = "txtMarketValueReport";
            this.txtMarketValueReport.Size = new System.Drawing.Size(181, 22);
            this.txtMarketValueReport.TabIndex = 50;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(330, 133);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(138, 17);
            this.label75.TabIndex = 37;
            this.label75.Text = "Market Value Report";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(330, 173);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(148, 17);
            this.label80.TabIndex = 38;
            this.label80.Text = "Mortgage Registration";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(330, 207);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(78, 17);
            this.label81.TabIndex = 39;
            this.label81.Text = "Advice Fee";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(330, 244);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(106, 17);
            this.label82.TabIndex = 43;
            this.label82.Text = "Mortgage Deed";
            // 
            // tabLoanParts
            // 
            this.tabLoanParts.Controls.Add(this.groupBox5);
            this.tabLoanParts.Location = new System.Drawing.Point(4, 25);
            this.tabLoanParts.Name = "tabLoanParts";
            this.tabLoanParts.Size = new System.Drawing.Size(1225, 574);
            this.tabLoanParts.TabIndex = 7;
            this.tabLoanParts.Text = "Loan Parts";
            this.tabLoanParts.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Silver;
            this.groupBox5.Controls.Add(this.cbNoLoanParts);
            this.groupBox5.Controls.Add(this.txtLoanPartID);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.cbLoanPartsType);
            this.groupBox5.Controls.Add(this.dpLoanpartsEndDate);
            this.groupBox5.Controls.Add(this.txtLoanpartsFixedInterestDuration);
            this.groupBox5.Controls.Add(this.txtLoanPartsValue);
            this.groupBox5.Controls.Add(this.label86);
            this.groupBox5.Controls.Add(this.label87);
            this.groupBox5.Controls.Add(this.label88);
            this.groupBox5.Controls.Add(this.label89);
            this.groupBox5.Location = new System.Drawing.Point(15, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1194, 550);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Assets";
            // 
            // cbNoLoanParts
            // 
            this.cbNoLoanParts.AutoSize = true;
            this.cbNoLoanParts.Location = new System.Drawing.Point(385, 49);
            this.cbNoLoanParts.Name = "cbNoLoanParts";
            this.cbNoLoanParts.Size = new System.Drawing.Size(155, 21);
            this.cbNoLoanParts.TabIndex = 76;
            this.cbNoLoanParts.Text = "No Loan Parts Data";
            this.cbNoLoanParts.UseVisualStyleBackColor = true;
            this.cbNoLoanParts.CheckStateChanged += new System.EventHandler(this.cbNoLoanParts_CheckStateChanged);
            // 
            // txtLoanPartID
            // 
            this.txtLoanPartID.Location = new System.Drawing.Point(534, 138);
            this.txtLoanPartID.Multiline = true;
            this.txtLoanPartID.Name = "txtLoanPartID";
            this.txtLoanPartID.Size = new System.Drawing.Size(234, 22);
            this.txtLoanPartID.TabIndex = 74;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(382, 138);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(79, 17);
            this.label32.TabIndex = 73;
            this.label32.Text = "LoanPartID";
            // 
            // cbLoanPartsType
            // 
            this.cbLoanPartsType.FormattingEnabled = true;
            this.cbLoanPartsType.Location = new System.Drawing.Point(534, 170);
            this.cbLoanPartsType.Name = "cbLoanPartsType";
            this.cbLoanPartsType.Size = new System.Drawing.Size(234, 24);
            this.cbLoanPartsType.TabIndex = 72;
            // 
            // dpLoanpartsEndDate
            // 
            this.dpLoanpartsEndDate.Location = new System.Drawing.Point(534, 208);
            this.dpLoanpartsEndDate.Name = "dpLoanpartsEndDate";
            this.dpLoanpartsEndDate.Size = new System.Drawing.Size(234, 22);
            this.dpLoanpartsEndDate.TabIndex = 71;
            // 
            // txtLoanpartsFixedInterestDuration
            // 
            this.txtLoanpartsFixedInterestDuration.Location = new System.Drawing.Point(534, 284);
            this.txtLoanpartsFixedInterestDuration.Multiline = true;
            this.txtLoanpartsFixedInterestDuration.Name = "txtLoanpartsFixedInterestDuration";
            this.txtLoanpartsFixedInterestDuration.Size = new System.Drawing.Size(234, 22);
            this.txtLoanpartsFixedInterestDuration.TabIndex = 56;
            // 
            // txtLoanPartsValue
            // 
            this.txtLoanPartsValue.Location = new System.Drawing.Point(534, 247);
            this.txtLoanPartsValue.Multiline = true;
            this.txtLoanPartsValue.Name = "txtLoanPartsValue";
            this.txtLoanPartsValue.Size = new System.Drawing.Size(234, 22);
            this.txtLoanPartsValue.TabIndex = 54;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(382, 173);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(40, 17);
            this.label86.TabIndex = 37;
            this.label86.Text = "Type";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(382, 213);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(67, 17);
            this.label87.TabIndex = 38;
            this.label87.Text = "End Date";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(382, 247);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(44, 17);
            this.label88.TabIndex = 39;
            this.label88.Text = "Value";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(382, 284);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(150, 17);
            this.label89.TabIndex = 43;
            this.label89.Text = "Fixed Interest Duration";
            // 
            // tabTermLifeInsurances
            // 
            this.tabTermLifeInsurances.Controls.Add(this.groupBox6);
            this.tabTermLifeInsurances.Location = new System.Drawing.Point(4, 25);
            this.tabTermLifeInsurances.Name = "tabTermLifeInsurances";
            this.tabTermLifeInsurances.Size = new System.Drawing.Size(1225, 574);
            this.tabTermLifeInsurances.TabIndex = 9;
            this.tabTermLifeInsurances.Text = "Term Life insurances";
            this.tabTermLifeInsurances.UseVisualStyleBackColor = true;
            this.tabTermLifeInsurances.Visible = false;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Silver;
            this.groupBox6.Controls.Add(this.txttliInitialValue);
            this.groupBox6.Controls.Add(this.label95);
            this.groupBox6.Controls.Add(this.cbNoTermLifeInsurance);
            this.groupBox6.Controls.Add(this.txtTLIForLoanPArt);
            this.groupBox6.Controls.Add(this.label93);
            this.groupBox6.Controls.Add(this.txtTLIInsuredValue);
            this.groupBox6.Controls.Add(this.txtTLIPayment);
            this.groupBox6.Controls.Add(this.label74);
            this.groupBox6.Controls.Add(this.label92);
            this.groupBox6.Controls.Add(this.txtTLIOwner);
            this.groupBox6.Controls.Add(this.txtTLIID);
            this.groupBox6.Controls.Add(this.label84);
            this.groupBox6.Controls.Add(this.label91);
            this.groupBox6.Location = new System.Drawing.Point(15, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1194, 550);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Assets";
            // 
            // txttliInitialValue
            // 
            this.txttliInitialValue.Location = new System.Drawing.Point(555, 143);
            this.txttliInitialValue.Multiline = true;
            this.txttliInitialValue.Name = "txttliInitialValue";
            this.txttliInitialValue.Size = new System.Drawing.Size(280, 22);
            this.txttliInitialValue.TabIndex = 69;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(338, 148);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(80, 17);
            this.label95.TabIndex = 68;
            this.label95.Text = "Initial Value";
            // 
            // cbNoTermLifeInsurance
            // 
            this.cbNoTermLifeInsurance.AutoSize = true;
            this.cbNoTermLifeInsurance.Location = new System.Drawing.Point(320, 52);
            this.cbNoTermLifeInsurance.Name = "cbNoTermLifeInsurance";
            this.cbNoTermLifeInsurance.Size = new System.Drawing.Size(212, 21);
            this.cbNoTermLifeInsurance.TabIndex = 67;
            this.cbNoTermLifeInsurance.Text = "No Term Life Insurance Data";
            this.cbNoTermLifeInsurance.UseVisualStyleBackColor = true;
            this.cbNoTermLifeInsurance.CheckStateChanged += new System.EventHandler(this.cbNoTermLifeInsurance_CheckStateChanged);
            // 
            // txtTLIForLoanPArt
            // 
            this.txtTLIForLoanPArt.Enabled = false;
            this.txtTLIForLoanPArt.Location = new System.Drawing.Point(269, 372);
            this.txtTLIForLoanPArt.Multiline = true;
            this.txtTLIForLoanPArt.Name = "txtTLIForLoanPArt";
            this.txtTLIForLoanPArt.Size = new System.Drawing.Size(280, 22);
            this.txtTLIForLoanPArt.TabIndex = 63;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Enabled = false;
            this.label93.Location = new System.Drawing.Point(52, 377);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(95, 17);
            this.label93.TabIndex = 61;
            this.label93.Text = "For Loan Part";
            // 
            // txtTLIInsuredValue
            // 
            this.txtTLIInsuredValue.Location = new System.Drawing.Point(555, 171);
            this.txtTLIInsuredValue.Multiline = true;
            this.txtTLIInsuredValue.Name = "txtTLIInsuredValue";
            this.txtTLIInsuredValue.Size = new System.Drawing.Size(280, 22);
            this.txtTLIInsuredValue.TabIndex = 60;
            // 
            // txtTLIPayment
            // 
            this.txtTLIPayment.Enabled = false;
            this.txtTLIPayment.Location = new System.Drawing.Point(269, 296);
            this.txtTLIPayment.Multiline = true;
            this.txtTLIPayment.Name = "txtTLIPayment";
            this.txtTLIPayment.Size = new System.Drawing.Size(280, 22);
            this.txtTLIPayment.TabIndex = 59;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Enabled = false;
            this.label74.Location = new System.Drawing.Point(52, 301);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(63, 17);
            this.label74.TabIndex = 57;
            this.label74.Text = "Payment";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(338, 176);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(95, 17);
            this.label92.TabIndex = 58;
            this.label92.Text = "Insured Value";
            // 
            // txtTLIOwner
            // 
            this.txtTLIOwner.Enabled = false;
            this.txtTLIOwner.Location = new System.Drawing.Point(269, 263);
            this.txtTLIOwner.Multiline = true;
            this.txtTLIOwner.Name = "txtTLIOwner";
            this.txtTLIOwner.Size = new System.Drawing.Size(280, 22);
            this.txtTLIOwner.TabIndex = 56;
            // 
            // txtTLIID
            // 
            this.txtTLIID.Enabled = false;
            this.txtTLIID.Location = new System.Drawing.Point(269, 225);
            this.txtTLIID.Multiline = true;
            this.txtTLIID.Name = "txtTLIID";
            this.txtTLIID.Size = new System.Drawing.Size(280, 22);
            this.txtTLIID.TabIndex = 50;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Enabled = false;
            this.label84.Location = new System.Drawing.Point(52, 230);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(21, 17);
            this.label84.TabIndex = 37;
            this.label84.Text = "ID";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Enabled = false;
            this.label91.Location = new System.Drawing.Point(52, 268);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(49, 17);
            this.label91.TabIndex = 43;
            this.label91.Text = "Owner";
            // 
            // tabApplicationDetails
            // 
            this.tabApplicationDetails.Controls.Add(this.groupBox7);
            this.tabApplicationDetails.Location = new System.Drawing.Point(4, 25);
            this.tabApplicationDetails.Name = "tabApplicationDetails";
            this.tabApplicationDetails.Size = new System.Drawing.Size(1225, 574);
            this.tabApplicationDetails.TabIndex = 10;
            this.tabApplicationDetails.Text = "Application Details";
            this.tabApplicationDetails.UseVisualStyleBackColor = true;
            this.tabApplicationDetails.Visible = false;
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Silver;
            this.groupBox7.Controls.Add(this.cbNoApplicationDetails);
            this.groupBox7.Controls.Add(this.cbApplicationType);
            this.groupBox7.Controls.Add(this.txtAdviceStatus);
            this.groupBox7.Controls.Add(this.label85);
            this.groupBox7.Controls.Add(this.label90);
            this.groupBox7.Location = new System.Drawing.Point(15, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(1194, 550);
            this.groupBox7.TabIndex = 7;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Assets";
            // 
            // cbNoApplicationDetails
            // 
            this.cbNoApplicationDetails.AutoSize = true;
            this.cbNoApplicationDetails.Location = new System.Drawing.Point(320, 69);
            this.cbNoApplicationDetails.Name = "cbNoApplicationDetails";
            this.cbNoApplicationDetails.Size = new System.Drawing.Size(202, 21);
            this.cbNoApplicationDetails.TabIndex = 75;
            this.cbNoApplicationDetails.Text = "No Application Details Data";
            this.cbNoApplicationDetails.UseVisualStyleBackColor = true;
            this.cbNoApplicationDetails.CheckStateChanged += new System.EventHandler(this.cbNoApplicationDetails_CheckStateChanged);
            // 
            // cbApplicationType
            // 
            this.cbApplicationType.FormattingEnabled = true;
            this.cbApplicationType.Location = new System.Drawing.Point(534, 232);
            this.cbApplicationType.Name = "cbApplicationType";
            this.cbApplicationType.Size = new System.Drawing.Size(181, 24);
            this.cbApplicationType.TabIndex = 73;
            // 
            // txtAdviceStatus
            // 
            this.txtAdviceStatus.Location = new System.Drawing.Point(534, 173);
            this.txtAdviceStatus.Multiline = true;
            this.txtAdviceStatus.Name = "txtAdviceStatus";
            this.txtAdviceStatus.Size = new System.Drawing.Size(181, 22);
            this.txtAdviceStatus.TabIndex = 50;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(317, 178);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(94, 17);
            this.label85.TabIndex = 37;
            this.label85.Text = "Advice Status";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(317, 235);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(113, 17);
            this.label90.TabIndex = 43;
            this.label90.Text = "Application Type";
            // 
            // cbSaveInputData
            // 
            this.cbSaveInputData.AutoSize = true;
            this.cbSaveInputData.Location = new System.Drawing.Point(317, 312);
            this.cbSaveInputData.Name = "cbSaveInputData";
            this.cbSaveInputData.Size = new System.Drawing.Size(233, 21);
            this.cbSaveInputData.TabIndex = 5;
            this.cbSaveInputData.Text = "Save input data to file for re-use";
            this.cbSaveInputData.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1275, 656);
            this.Controls.Add(this.btnGenerateJSON);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "JSON Generator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabPerson.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabIncomes.ResumeLayout(false);
            this.gbYearlyIncomes.ResumeLayout(false);
            this.gbYearlyIncomes.PerformLayout();
            this.gbIncomes.ResumeLayout(false);
            this.gbIncomes.PerformLayout();
            this.tabPensions.ResumeLayout(false);
            this.gbPensions.ResumeLayout(false);
            this.gbPensions.PerformLayout();
            this.tabExpenses.ResumeLayout(false);
            this.gbExpenses.ResumeLayout(false);
            this.gbExpenses.PerformLayout();
            this.tabAssets.ResumeLayout(false);
            this.gbAssets.ResumeLayout(false);
            this.gbAssets.PerformLayout();
            this.tabLoans.ResumeLayout(false);
            this.gbLoans.ResumeLayout(false);
            this.gbLoans.PerformLayout();
            this.tabRealProperty.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.gbRealPropertyHouse.ResumeLayout(false);
            this.gbRealPropertyHouse.PerformLayout();
            this.tabFinanceConstruction.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabLoanParts.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabTermLifeInsurances.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabApplicationDetails.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGenerateJSON;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPerson;
        private System.Windows.Forms.TabPage tabIncomes;
        private System.Windows.Forms.TabPage tabPensions;
        private System.Windows.Forms.TabPage tabExpenses;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cbPersonType;
        private System.Windows.Forms.ComboBox cbGender;
        private System.Windows.Forms.ComboBox cbMaritalStatus;
        private System.Windows.Forms.ComboBox cbEverDivorced;
        private System.Windows.Forms.ComboBox cbSmoker;
        private System.Windows.Forms.TextBox txtBirthSurname;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtSocialSecurityNumber;
        private System.Windows.Forms.TextBox txtDiscountAOWYears;
        private System.Windows.Forms.TextBox txtNationality;
        private System.Windows.Forms.TextBox txtInitials;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.TextBox txtPrefix;
        private System.Windows.Forms.TextBox txtPersonID;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtStreetName;
        private System.Windows.Forms.TextBox txtHouseNumber;
        private System.Windows.Forms.TextBox txtSuffix;
        private System.Windows.Forms.TextBox txtPostalCode;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.DateTimePicker dpDivorceDate;
        private System.Windows.Forms.DateTimePicker dpDateOfBirth;
        private System.Windows.Forms.TabPage tabAssets;
        private System.Windows.Forms.TabPage tabLoans;
        private System.Windows.Forms.TabPage tabRealProperty;
        private System.Windows.Forms.TabPage tabLoanParts;
        private System.Windows.Forms.TabPage tabFinanceConstruction;
        private System.Windows.Forms.TabPage tabTermLifeInsurances;
        private System.Windows.Forms.TabPage tabApplicationDetails;
        private System.Windows.Forms.GroupBox gbIncomes;
        private System.Windows.Forms.DateTimePicker dpIncomeStartDate;
        private System.Windows.Forms.ComboBox cbIncomesType;
        private System.Windows.Forms.TextBox txtIncomesPArtTimePercentage;
        private System.Windows.Forms.TextBox txtIncomesOwner;
        private System.Windows.Forms.TextBox txtIncomesID;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.DateTimePicker dpIncomeEndDate;
        private System.Windows.Forms.GroupBox gbYearlyIncomes;
        private System.Windows.Forms.TextBox txtYearlyIncomeEndOfYearBenefit;
        private System.Windows.Forms.TextBox txtYearlyIncomeHolidayAllowanceValue;
        private System.Windows.Forms.TextBox txtYEarlyIncomeYEar;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtYEarlyIncomeOvertimeValue;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtYearlyIncomeProvisionValue;
        private System.Windows.Forms.TextBox txtYearlyIncomeExtraMonth;
        private System.Windows.Forms.TextBox txtYearlyIncomeIrregularityPayment;
        private System.Windows.Forms.TextBox txtYearlyIncomeValue;
        private System.Windows.Forms.GroupBox gbPensions;
        private System.Windows.Forms.DateTimePicker dpPensionStartDate;
        private System.Windows.Forms.ComboBox cbPensionType;
        private System.Windows.Forms.TextBox txtPensionValue;
        private System.Windows.Forms.TextBox txtPensionOwner;
        private System.Windows.Forms.TextBox txtPensionID;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.GroupBox gbExpenses;
        private System.Windows.Forms.ComboBox cbExpenseType;
        private System.Windows.Forms.TextBox txtExpenseMade;
        private System.Windows.Forms.TextBox txtExpenseOwner;
        private System.Windows.Forms.TextBox txtExpenseID;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.GroupBox gbAssets;
        private System.Windows.Forms.ComboBox cbAssetType;
        private System.Windows.Forms.TextBox txtAssetValue;
        private System.Windows.Forms.TextBox txtAssetOwner;
        private System.Windows.Forms.TextBox txtAssetID;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox txtAssetInterestRate;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.GroupBox gbLoans;
        private System.Windows.Forms.ComboBox cbLoanType;
        private System.Windows.Forms.TextBox txtLoanAmount;
        private System.Windows.Forms.TextBox txtLoanOwner;
        private System.Windows.Forms.TextBox txtLoanID;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtHouseStreetName;
        private System.Windows.Forms.TextBox txtHouseHouseNumber;
        private System.Windows.Forms.TextBox txtHousePostalCode;
        private System.Windows.Forms.TextBox txtHouseCity;
        private System.Windows.Forms.TextBox txtHouseMunicipality;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.GroupBox gbRealPropertyHouse;
        private System.Windows.Forms.ComboBox cbHouseType;
        private System.Windows.Forms.TextBox txtHouseProjectName;
        private System.Windows.Forms.TextBox txtHouseProjectNumber;
        private System.Windows.Forms.TextBox txtHouseID;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox txtContractorSum;
        private System.Windows.Forms.TextBox txtHouseOverdueMaintenance;
        private System.Windows.Forms.TextBox txtHouseEconomicValue;
        private System.Windows.Forms.TextBox txtHouseEcoValueAfterImprovements;
        private System.Windows.Forms.TextBox txtHouseGroundPrice;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.DateTimePicker dpHousePurchaseDate;
        private System.Windows.Forms.TextBox txtHouseExtraWorkContractor;
        private System.Windows.Forms.TextBox txtLeaseHoldID;
        private System.Windows.Forms.TextBox txtLeaseHoldValue;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.DateTimePicker dpLeaseHoldEndDate;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox txtMortGageDeed;
        private System.Windows.Forms.TextBox txtAdviceFee;
        private System.Windows.Forms.TextBox txtMarketValueReport;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox txtMortgageRegistration;
        private System.Windows.Forms.TextBox txtMortgageTotal;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.DateTimePicker dpMortgageStartDate;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DateTimePicker dpLoanpartsEndDate;
        private System.Windows.Forms.TextBox txtLoanpartsFixedInterestDuration;
        private System.Windows.Forms.TextBox txtLoanPartsValue;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtAdviceStatus;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.ComboBox cbLeaseType;
        private System.Windows.Forms.ComboBox cbLoanPartsType;
        private System.Windows.Forms.ComboBox cbApplicationType;
        private System.Windows.Forms.TextBox txtLoanPartID;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtTLIForLoanPArt;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox txtTLIInsuredValue;
        private System.Windows.Forms.TextBox txtTLIPayment;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox txtTLIOwner;
        private System.Windows.Forms.TextBox txtTLIID;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox txtFileCreationPath;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ComboBox cbHouseObjectType;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.CheckBox cbNoIncomesData;
        private System.Windows.Forms.CheckBox cbNoYearlyIncomesData;
        private System.Windows.Forms.CheckBox cbNoPensionData;
        private System.Windows.Forms.CheckBox cbNoExpensesData;
        private System.Windows.Forms.CheckBox cbNoAssetsData;
        private System.Windows.Forms.CheckBox cbNoLoansData;
        private System.Windows.Forms.CheckBox cbNoHouseData;
        private System.Windows.Forms.CheckBox cbNoLeaseHoldData;
        private System.Windows.Forms.CheckBox cbNoFinanceConstructionData;
        private System.Windows.Forms.CheckBox cbNoLoanParts;
        private System.Windows.Forms.CheckBox cbNoTermLifeInsurance;
        private System.Windows.Forms.CheckBox cbNoApplicationDetails;
        private System.Windows.Forms.TextBox txttliInitialValue;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.CheckBox cbSaveInputData;
    }
}

