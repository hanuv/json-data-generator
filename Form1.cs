﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JsonTestData_Generator
{
    public partial class Form1 : Form
    {

        #region variables

        #endregion

        Worker oWorker = new Worker();


        
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            saveDirectory();

                          
            //    if (txtFileCreationPath.Text == "")
            //    {
            //        MessageBox.Show("Please select a path for JSON files to be stored");
            //    }                    
            //else 
            //{
                
            //    oWorker.BuildJSONString(personID(), personType(), personInitials(), personFirstName(), personSurName(), personPrefix(),
            //    personBirthSurname(), Email(), Gender(), DateOfBirth(), MaritalStatus(), EverDivorced(), DivorceDate(), SocialSecurityNumber(),
            //    DiscountAOWYears(), Smoker(), Nationality(), addressStreet(), addressHouseNumber(), addressPostalCode(), addressCity(),
            //    IncomesID(), incomesType(), incomesOwner(), incomesStartDate(), incomesEndDate(), incomesPartTimePercentage(),
            //    yearlyIncomesYear(), yearlyIncomesValue(), yearlyIncomesHolidayAllowanceValue(), yearlyIncomesIrregularityPayment(),
            //    yearlyIncomesExtraMonth(), yearlyIncomesProvisionValue(), yearlyIncomesEndOfYearBenefitValue(), yearlyIncomesOverTimeValue(),
            //    pensionID(), pensionType(), pensionOwner(), pensionStartDate(), pensionValue(),
            //    expensesID(), expensesType(), expensesOwner(), ExpenseMade(),
            //    assetID(), assetType(), assetOwner(), assetValue(), assetInterestRate(),
            //    loanID(), loanType(), loanOwner(), loanAmount(),
            //    houseID(), houseType(), housePurchaseDate(), houseObjectType(), houseProjectName(), houseProjectNumber(), houseAddressStreet(), houseAddressHouseNumber(),
            //    houseAddressPostalCode(), houseAddressCity(), houseAddressMunicipality(), houseGroundPrice(), houseExtraWorkContractor(), houseContractorSum(),
            //    houseOverDueMaintenance(), houseEconomicValue(), houseEconomicValueAfterImprovements(),
            //    leaseHoldID(), leaseHoldType(), leaseHoldValue(), leaseHoldEndDate(),
            //    fcMarketValueReport(), fcMortgageRegistration(), fcAdviceFee(), fcMortgageDeed(), fcStartDate(), fcMortgageTotal(),
            //    loanpartsID(), loanpartsType(), loanpartsEndDate(), loanpartsValue(), loanpartsFixedInterestDuration(),
            //   tliInitialValue(), tliInsuredValue(),
            //    adviceStatus(), applicationType());

            //    MessageBox.Show("JSON file created successfully in: " + txtFileCreationPath.Text);
            //}


        }

        #region CLick events
        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void gbYearlyIncomes_Enter(object sender, EventArgs e)
        {

        }

        private void txtIncomesID_Validating(object sender, CancelEventArgs e)
        {
            string t = this.gbIncomes.Text.ToString();
            if (t == "")
                MessageBox.Show("Hello");
        }

        private void gbRealPropertyHouse_Enter(object sender, EventArgs e)
        {

        }

        


        #endregion

        #region Person Attributes

        public string personID()
        {
            return oWorker.personID = txtPersonID.Text;
        }
        public string personType()
        {
            return oWorker.personType = cbPersonType.Text;
             
        }
        public string personInitials()
        {
            return oWorker.personInitials = txtInitials.Text;
        }
        public string personFirstName()
        {
            return oWorker.personFirstName = txtFirstName.Text;
        }
        public string personSurName()
        {
            return oWorker.personSurName = txtLastName.Text;
        }
        public string personPrefix()
        {
            return oWorker.personPrefix = txtPrefix.Text;
        }
        public string personBirthSurname()
        {
            return oWorker.personBirthSurname = txtBirthSurname.Text;
        }
        public string Email()
        {
            return oWorker.Email = txtEmail.Text;
        }
        public string Gender()
        {
            return oWorker.Gender = cbGender.Text;
        }
        public string DateOfBirth()
        {
            return oWorker.DateOfBirth = dpDateOfBirth.Text;
        }
        public string MaritalStatus()
        {
            return oWorker.MaritalStatus = cbMaritalStatus.Text;
        }
        public string EverDivorced()
        {
            return oWorker.EverDivorced = cbEverDivorced.Text;
        }
        public string DivorceDate()
        {
            if (cbEverDivorced.Text == "No")
            {
                return oWorker.DivorceDate = "";
            }

            else

                return oWorker.DivorceDate = dpDivorceDate.Text;
            
        }
        public string SocialSecurityNumber()
        {
            return oWorker.SocialSecurityNumber = txtSocialSecurityNumber.Text;
        }
        public string DiscountAOWYears()
        {
            return oWorker.DiscountAOWYears = txtDiscountAOWYears.Text;
        }
        public string Smoker()
        {
            return oWorker.Smoker = cbSmoker.Text;
        }
        public string Nationality()
        {
            return oWorker.Nationality = txtNationality.Text;
        }
        public string addressStreet()
        {
            return oWorker.addressStreet = txtStreetName.Text;
        }
        public string addressHouseNumber()
        {
            return oWorker.addressHouseNumber = txtHouseNumber.Text;
        }
        public string addressSuffix()
        {
            return oWorker.addressSuffix = txtSuffix.Text;
        }
        public string addressPostalCode()
        {
            return oWorker.addressPostalCode = txtPostalCode.Text;
        }
        public string addressCity()
        {
            return oWorker.addressCity = txtCity.Text;
        }
        #endregion

        #region Incmomes Attributes
        public string IncomesID()
        {
            return oWorker.incomesID = txtIncomesID.Text;
        }

        public string incomesType()
        {
            return oWorker.incomesType = cbIncomesType.Text;
        }

        public string incomesOwner()
        {
            return oWorker.incomesOwner = txtIncomesOwner.Text;
        }

        public string incomesStartDate()
        {
            return oWorker.incomesStartDate = dpIncomeStartDate.Text;
        }

        public string incomesEndDate()
        {
            return oWorker.incomesEndDate = dpIncomeEndDate.Text;
        }

        public string incomesPartTimePercentage()
        {
            return oWorker.incomesPartTimePercentage = txtIncomesPArtTimePercentage.Text;
        }

        public string yearlyIncomesYear()
        {
            return oWorker.yearlyIncomesYear = txtYEarlyIncomeYEar.Text;
        }

        public string yearlyIncomesValue()
        {
            return oWorker.yearlyIncomesValue = txtYearlyIncomeValue.Text;
        }

        public string yearlyIncomesHolidayAllowanceValue()
        {
            return oWorker.yearlyIncomesHolidayAllowanceValue = txtYearlyIncomeHolidayAllowanceValue.Text;
        }

        public string yearlyIncomesIrregularityPayment()
        {
            return oWorker.yearlyIncomesIrregularityPayment = txtYearlyIncomeIrregularityPayment.Text;
        }

        public string yearlyIncomesExtraMonth()
        {
            return oWorker.yearlyIncomesExtraMonth = txtYearlyIncomeExtraMonth.Text;
        }

        public string yearlyIncomesProvisionValue()
        {
            return oWorker.yearlyIncomesProvisionValue = txtYearlyIncomeProvisionValue.Text;
        }

        public string yearlyIncomesEndOfYearBenefitValue()
        {
            return oWorker.yearlyIncomesEndOfYearBenefitValue = txtYearlyIncomeEndOfYearBenefit.Text;
        }

        public string yearlyIncomesOverTimeValue()
        {
            return oWorker.yearlyIncomesOverTimeValue = txtYEarlyIncomeOvertimeValue.Text;
        }




        #endregion

        #region Pension Attributes

        public string pensionID()
        {
            return oWorker.pensionID = txtPensionID.Text;
        }
        public string pensionType()
        {
            return oWorker.pensionType = cbPensionType.Text;
        }
        public string pensionOwner()
        {
            return oWorker.pensionOwner = txtPensionOwner.Text;
        }
        public string pensionStartDate()
        {
            return oWorker.pensionStartDate = dpPensionStartDate.Text;
        }
        public string pensionValue()
        {
            return oWorker.pensionValue = txtPensionValue.Text;
        }


        #endregion

        #region Expenses Attributes
        public string expensesID()
        {
            return oWorker.expensesID = txtExpenseID.Text;
        }
        public string expensesType()
        {
            return oWorker.expensesType = cbExpenseType.Text;
        }
        public string expensesOwner()
        {
            return oWorker.expensesOwner = txtExpenseOwner.Text;
        }
        public string ExpenseMade()
        {
            return oWorker.ExpenseMade = txtExpenseMade.Text;
        }
        #endregion

        #region Asset Attributes
        public string assetID()
        {
            return oWorker.assetID = txtAssetID.Text;
        }
        public string assetType()
        {
            return oWorker.assetType = cbAssetType.Text;
        }
        public string assetOwner()
        {
            return oWorker.assetOwner = txtAssetOwner.Text;
        }
        public string assetValue()
        {
            return oWorker.assetValue = txtAssetValue.Text;
        }
        public string assetInterestRate()
        {
            return oWorker.assetInterestRate = txtAssetInterestRate.Text;
        }
        #endregion

        #region Loan Attributes
        public string loanID()
        {
            return oWorker.loanID = txtLoanID.Text;
        }
        public string loanType()
        {
            return oWorker.loanType = cbLoanType.Text;
        }
        public string loanOwner()
        {
            return oWorker.loanOwner = txtLoanOwner.Text;
        }
        public string loanAmount()
        {
            return oWorker.loanAmount = txtLoanAmount.Text;
        }
        #endregion

        #region RealProperty House Attributes
        public string houseID()
        {
            return oWorker.houseID = txtHouseID.Text;
        }
        public string houseType()
        {
            return oWorker.houseType = cbHouseType.Text;

        }
        public string houseObjectType()
        {
            return oWorker.houseObjectType = cbHouseObjectType.Text;
        }
        public string houseProjectName()
        {
            return oWorker.houseProjectName = txtHouseProjectName.Text;
        }
        public string houseProjectNumber()
        {
            return oWorker.houseProjectNumber = txtHouseProjectNumber.Text;
        }
        public string housePurchaseDate()
        {
            return oWorker.housePurchaseDate = dpHousePurchaseDate.Text;
        }
        public string houseAddressStreet()
        {
            return oWorker.houseAddressStreet = txtHouseStreetName.Text;
        }
        public string houseAddressHouseNumber()
        {
            return oWorker.houseAddressHouseNumber = txtHouseHouseNumber.Text;
        }
        public string houseAddressPostalCode()
        {
            return oWorker.houseAddressPostalCode = txtHousePostalCode.Text;
        }
        public string houseAddressCity()
        {
            return oWorker.houseAddressCity = txtHouseCity.Text;
        }
        public string houseAddressMunicipality()
        {
            return oWorker.houseAddressMunicipality = txtHouseMunicipality.Text;
        }
        public string houseGroundPrice()
        {
                return oWorker.houseGroundPrice = txtHouseGroundPrice.Text;
        }
        public string houseExtraWorkContractor()
        {
            return oWorker.houseExtraWorkContractor = txtHouseExtraWorkContractor.Text;
        }
        public string houseContractorSum()
        {
            return oWorker.houseContractorSum = txtContractorSum.Text;
        }
        public string houseOverDueMaintenance()
        {
            return oWorker.houseOverDueMaintenance = txtHouseOverdueMaintenance.Text;
        }
        public string houseEconomicValue()
        {
            return oWorker.houseEconomicValue = txtHouseEconomicValue.Text;
        }
        public string houseEconomicValueAfterImprovements()
        {
            return oWorker.houseEconomicValueAfterImprovements = txtHouseEcoValueAfterImprovements.Text;
        }
        #endregion

        #region Realproperty Leasehold Attributes
        public string leaseHoldID()
        {
            return oWorker.leaseHoldID = txtLeaseHoldID.Text;
        }
        public string leaseHoldType()
        {
            return oWorker.leaseHoldType = cbLeaseType.Text;
        }
        public string leaseHoldValue()
        {
            return oWorker.leaseHoldValue = txtLeaseHoldValue.Text;
        }
        public string leaseHoldEndDate()
        {
            return oWorker.leaseHoldEndDate = dpLeaseHoldEndDate.Text;
        }
        #endregion

        #region FinanceConstruction Attributes Attributes
        public string fcMarketValueReport()
        {
            return oWorker.fcMarketValueReport = txtMarketValueReport.Text;
        }
        public string fcMortgageRegistration()
        {
            return oWorker.fcMortgageRegistration = txtMortgageRegistration.Text;
        }
        public string fcAdviceFee()
        {
            return oWorker.fcAdviceFee = txtAdviceFee.Text;
        }
        public string fcMortgageDeed()
        {
            return oWorker.fcMortgageDeed = txtMortGageDeed.Text;
        }
        public string fcStartDate()
        {
            return oWorker.fcStartDate = dpMortgageStartDate.Text;
        }
        public string fcMortgageTotal()
        {
            return oWorker.fcMortgageTotal = txtMortgageTotal.Text;
        }
        #endregion

        #region Loanparts Attributes
        public string loanpartsID()
        {
            return oWorker.loanpartsID = txtLoanPartID.Text;
        }
        public string loanpartsType()
        {
            return oWorker.loanpartsType = cbLoanPartsType.Text;
        }
        public string loanpartsEndDate()
        {
            return oWorker.loanpartsEndDate = dpLoanpartsEndDate.Text;
        }
        public string loanpartsValue()
        {
            return oWorker.loanpartsValue = txtLoanPartsValue.Text;
        }
        public string loanpartsFixedInterestDuration()
        {
            return oWorker.loanpartsFixedInterestDuration = txtLoanpartsFixedInterestDuration.Text;
        }
        #endregion

        #region Termlife insurances Attributes
        public string tliID()
        {
            return oWorker.tliID = txtTLIID.Text;
        }
        public string tliOwner()
        {
            return oWorker.tliOwner = txtTLIOwner.Text;
        }
        public string tliPayment()
        {
            return oWorker.tliPayment = txtTLIPayment.Text;
        }
        public string tliInsuredValue()
        {
            return oWorker.tliInsuredValue = txtTLIInsuredValue.Text;
        }
        public string tliForLoanPart()
        {
            return oWorker.tliForLoanPart = txtTLIForLoanPArt.Text;
        }

        public string tliInitialValue()
        {
            return oWorker.tliInitialValue = txttliInitialValue.Text;
        }
        #endregion

        #region Application Details
        public string adviceStatus()
        {
            return oWorker.adviceStatus = txtAdviceStatus.Text;
        }
        public string applicationType()
        {
            return oWorker.applicationType = cbApplicationType.Text;
        }
        #endregion

        public string saveDirectory()
        {
            return oWorker.saveDirectory = txtFileCreationPath.Text;
        }

        #region Load Form
        private void Form1_Load(object sender, EventArgs e)
        {
            string personGuid;
            string incomeGUID;
            string pensionGUID;
            string expenseGUID;
            string assetGUID;
            string loanGUID;
            string houseGUID;
            string leaseGUID;
            string loanpartsGUID;
            string termLifeInsuranceID;

            personGuid = oWorker.generateNewGuid();
            incomeGUID = oWorker.generateNewGuid();
            pensionGUID = oWorker.generateNewGuid();
            expenseGUID = oWorker.generateNewGuid();
            assetGUID = oWorker.generateNewGuid();
            loanGUID = oWorker.generateNewGuid();
            houseGUID = oWorker.generateNewGuid();
            leaseGUID = oWorker.generateNewGuid();
            loanpartsGUID = oWorker.generateNewGuid();
            termLifeInsuranceID = oWorker.generateNewGuid();

            txtPersonID.Text = personGuid;
            txtIncomesOwner.Text = personGuid;
            txtPensionOwner.Text = personGuid;
            txtExpenseOwner.Text = personGuid;
            txtAssetOwner.Text = personGuid;
            txtLoanOwner.Text = personGuid;
            txtTLIOwner.Text = personGuid;

            txtIncomesID.Text = incomeGUID;
            txtPensionID.Text = pensionGUID;
            txtExpenseID.Text = expenseGUID;
            txtAssetID.Text = assetGUID;
            txtLoanID.Text = loanGUID;
            txtHouseID.Text = houseGUID;
            txtLeaseHoldID.Text = leaseGUID;
            txtLoanPartID.Text = loanpartsGUID;
            txtTLIID.Text = termLifeInsuranceID;
            txtTLIForLoanPArt.Text = termLifeInsuranceID;



            cbPersonType.Items.Add("Client");
            cbPersonType.Items.Add("Plient");

            cbGender.Items.Add("Male");
            cbGender.Items.Add("Female");

            cbMaritalStatus.Items.Add("Cohabiting");
            cbMaritalStatus.Items.Add("CohabitingWithContract");
            cbMaritalStatus.Items.Add("Divorced");
            cbMaritalStatus.Items.Add("Married");
            cbMaritalStatus.Items.Add("MarriedForeign");
            cbMaritalStatus.Items.Add("MarriedWithPrenup");
            cbMaritalStatus.Items.Add("RegisteredPartner");
            cbMaritalStatus.Items.Add("RegisteredPArtnerWithConditions");
            cbMaritalStatus.Items.Add("Single");
            cbMaritalStatus.Items.Add("Undefined");

            cbEverDivorced.Items.Add("Yes");
            cbEverDivorced.Items.Add("No");

            cbSmoker.Items.Add("True");
            cbSmoker.Items.Add("False");

            cbIncomesType.Items.Add("Permanent");
            cbIncomesType.Items.Add("TemporaryWithPositiveIntent");
            cbIncomesType.Items.Add("TemporaryWithoutPositiveIntent");
            cbIncomesType.Items.Add("FlexWorker");
            cbIncomesType.Items.Add("ZZP");
            cbIncomesType.Items.Add("Freelancer");
            cbIncomesType.Items.Add("Alimony");

            cbPensionType.Items.Add("DefinedBenefit");

            cbExpenseType.Items.Add("PaidAlimony");

            cbAssetType.Items.Add("SavingsAccount");

            cbLoanType.Items.Add("StudentLoan");
            cbLoanType.Items.Add("OtherLoan");

            cbHouseType.Items.Add("NewlyBuiltHouse");
            cbHouseType.Items.Add("ExistingHouse");

            cbHouseObjectType.Items.Add("FamilyHouse");
            cbHouseObjectType.Items.Add("FamilyHouseWithGarage");
            cbHouseObjectType.Items.Add("Apartment");
            cbHouseObjectType.Items.Add("ApartmentWithGarage");
            cbHouseObjectType.Items.Add("Storehouse");
            cbHouseObjectType.Items.Add("HouseWithCommercialSpace");
            cbHouseObjectType.Items.Add("Shop");
            cbHouseObjectType.Items.Add("Premises");
            cbHouseObjectType.Items.Add("RecreationalHouse");
            cbHouseObjectType.Items.Add("Farm");
            cbHouseObjectType.Items.Add("Pledge");
            cbHouseObjectType.Items.Add("AgriculturalLand");
            cbHouseObjectType.Items.Add("Garage");
            cbHouseObjectType.Items.Add("Plot");
            cbHouseObjectType.Items.Add("HouseBoat");
            cbHouseObjectType.Items.Add("RecreationalBoat");
            cbHouseObjectType.Items.Add("Other");

            cbLeaseType.Items.Add("Once");
            cbLeaseType.Items.Add("Scheduled");

            cbLoanPartsType.Items.Add("AnnuityMortgage");
            cbLoanPartsType.Items.Add("LinearMortgage");

            cbApplicationType.Items.Add("ExecutionOnly");
        }


        #endregion

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Text != "Configure" && txtFileCreationPath.Text != "")
            {
                btnGenerateJSON.Text = "Generate JSON";
            }
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            string filepath = "";

            DialogResult result = folderBrowserDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                filepath = folderBrowserDialog1.SelectedPath.ToString();
            }

            txtFileCreationPath.Text = filepath;
        }

        #region Checkbox events
        private void cbNoIncomesData_CheckStateChanged(object sender, EventArgs e)
        {
            Control Parent = cbNoIncomesData.Parent;
            disableControlsInGroupBox(cbNoIncomesData, Parent);
        }

        private void cbNoYearlyIncomesData_CheckStateChanged(object sender, EventArgs e)
        {
            Control Parent = cbNoYearlyIncomesData.Parent;
            disableControlsInGroupBox(cbNoYearlyIncomesData,Parent);
        }

        private void cbNoPensionData_CheckStateChanged(object sender, EventArgs e)
        {
            Control Parent = cbNoPensionData.Parent;
            disableControlsInGroupBox(cbNoPensionData,Parent);
        }

        private void cbNoExpensesData_CheckStateChanged(object sender, EventArgs e)
        {
            Control Parent = cbNoExpensesData.Parent;
            disableControlsInGroupBox(cbNoExpensesData,Parent);
        }

        private void cbNoAssetsData_CheckStateChanged(object sender, EventArgs e)
        {
            Control Parent = cbNoAssetsData.Parent;
            disableControlsInGroupBox(cbNoAssetsData,Parent);
        }

        private void cbNoLoansData_CheckStateChanged(object sender, EventArgs e)
        {
            Control Parent = cbNoLoansData.Parent;
            disableControlsInGroupBox(cbNoLoansData,Parent);
        }

        private void cbNoHouseData_CheckStateChanged(object sender, EventArgs e)
        {
            Control Parent = cbNoHouseData.Parent;
            disableControlsInGroupBox(cbNoHouseData,Parent);
        }

        private void cbNoLeaseHoldData_CheckStateChanged(object sender, EventArgs e)
        {
            Control Parent = cbNoLeaseHoldData.Parent;
            disableControlsInGroupBox(cbNoLeaseHoldData,Parent);
        }

        private void cbNoFinanceConstructionData_CheckStateChanged(object sender, EventArgs e)
        {
            Control Parent = cbNoFinanceConstructionData.Parent;
            disableControlsInGroupBox(cbNoFinanceConstructionData,Parent);
        }

        private void cbNoLoanParts_CheckStateChanged(object sender, EventArgs e)
        {
            Control Parent = cbNoLoanParts.Parent;
            disableControlsInGroupBox(cbNoLoanParts,Parent);
        }

        private void cbNoTermLifeInsurance_CheckStateChanged(object sender, EventArgs e)
        {
            Control Parent = cbNoTermLifeInsurance.Parent;
            disableControlsInGroupBox(cbNoTermLifeInsurance,Parent);
        }

        private void cbNoApplicationDetails_CheckStateChanged(object sender, EventArgs e)
        {
            Control Parent = cbNoApplicationDetails.Parent;
            disableControlsInGroupBox(cbNoApplicationDetails,Parent);
        }

        #endregion

        private void disableControlsInGroupBox(CheckBox cb, Control parentControl)
        { 
            foreach (Control control in parentControl.Controls)
            {
                TextBox txtbox = control as TextBox;

                if (cb.Checked == true)
                {
                    if (control is TextBox || control is ComboBox || control is DateTimePicker)
                    {
                        control.Enabled = false;
                    }
                }
                else if(cb.Checked == false)
                {
                    if (control is TextBox || control is ComboBox || control is DateTimePicker)
                    {
                        control.Enabled = true;
                    }
                }
                
            }
        }
       
    }
}
