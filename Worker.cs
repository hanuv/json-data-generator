﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonTestData_Generator
{
    class Worker
    {

        #region variables
        public string applicationID;
        //Person Data
        public string personID;
        public string personType;
        public string personInitials;
        public string personFirstName;
        public string personSurName;
        public string personPrefix;
        public string personBirthSurname;
        public string Email;
        public string Gender;
        public string DateOfBirth;
        public string MaritalStatus;
        public string EverDivorced;
        public string DivorceDate;
        public string SocialSecurityNumber;
        public string DiscountAOWYears;
        public string Smoker;
        public string Nationality;
        //Person Address Data
        public string addressStreet;
        public string addressHouseNumber;
        public string addressSuffix;
        public string addressPostalCode;
        public string addressCity;


        //Person Incomes
        public string incomesID;
        public string incomesType;
        public string incomesOwner;
        public string incomesStartDate;
        public string incomesEndDate;
        public string incomesPartTimePercentage;

        public string yearlyIncomesYear;
        public string yearlyIncomesValue;
        public string yearlyIncomesHolidayAllowanceValue;
        public string yearlyIncomesIrregularityPayment;
        public string yearlyIncomesExtraMonth;
        public string yearlyIncomesProvisionValue;
        public string yearlyIncomesEndOfYearBenefitValue;
        public string yearlyIncomesOverTimeValue;

        //Pensions
        public string pensionID;
        public string pensionType;
        public string pensionOwner;
        public string pensionStartDate;
        public string pensionValue;

        //Expenses
        public string expensesID;
        public string expensesType;
        public string expensesOwner;
        public string ExpenseMade;

        //Assets
        public string assetID;
        public string assetType;
        public string assetOwner;
        public string assetValue;
        public string assetInterestRate;

        //Loans
        public string loanID;
        public string loanType;
        public string loanOwner;
        public string loanAmount;

        //RealProperty
        //House
        public string houseID;
        public string houseType;
        public string houseObjectType;
        public string houseProjectName;
        public string houseProjectNumber;
        public string housePurchaseDate;
        public string houseAddressStreet;
        public string houseAddressHouseNumber;
        public string houseAddressPostalCode;
        public string houseAddressCity;
        public string houseAddressMunicipality;
        public string houseGroundPrice;
        public string houseExtraWorkContractor;
        public string houseContractorSum;
        public string houseOverDueMaintenance;
        public string houseEconomicValue;
        public string houseEconomicValueAfterImprovements;


        //RealProperty
        //LeaseHold
        public string leaseHoldID;
        public string leaseHoldType;
        public string leaseHoldValue;
        public string leaseHoldEndDate;

        //FinanceConstruction
        public string fcMarketValueReport;
        public string fcMortgageRegistration;
        public string fcAdviceFee;
        public string fcMortgageDeed;
        public string fcStartDate;
        public string fcMortgageTotal;

        //Loanparts
        public string loanpartsID;
        public string loanpartsType;
        public string loanpartsEndDate;
        public string loanpartsValue;
        public string loanpartsFixedInterestDuration;


        //TermLifeInsurances
        public string tliID;
        public string tliOwner;
        public string tliPayment;
        public string tliInsuredValue;
        public string tliForLoanPart;
        public string tliInitialValue;     


        //ApplicationDetails
        public string adviceStatus;
        public string applicationType;

        public string saveDirectory;
        #endregion

        public void writeToTextFile(string JSON)
        {
            string dir = "";
            string personid = "";

            personid = personID;
            dir = saveDirectory + @"\" + personid + ".txt";            
            
            System.IO.File.WriteAllText(dir, JSON);

            


        }


        public string generateNewGuid()
        {
            string newGuid = "";
            newGuid = Guid.NewGuid().ToString();

            return newGuid;
        }


        #region BuildJSONSTring
        public void BuildJSONString(string personID,string Type,string Initials, string firstName,string surName,string prefix,string birthSurname,string email, 
            string gender, string dateofbirth, string maritalstatus,string everdivorced, string divorcedate, string socialsecuritynumber, string discountaowyears,
            string smoker, string nationality, string addressstreet, string addresshousenumber,string postalcode,string addresscity,
            string incomesID, string incomesType, string incomesOwner, string incomesStartDate, string incomesEndDate, string incomesPartTimePercentage,
            string yearlyIncomesYear, string yearlyIncomesValue, string yearlyIncomesHolidayAllowanceValue, string yearlyIncomesIrregularityPayment, string yearlyIncomesExtraMonth,
            string yearlyIncomesProvisionValue, string EndOfYearBenefitValue, string OverTimeValue, string pensionID, string pensionType, string pensionOwner, string pensionStartDate,
            string pensionValue, string expenseID, string expenseType, string expenseOwner, string expenseMade, string assetID, string assetType, string assetOwner, string assetValue,
            string assetInterestRate, string loanID, string loanType, string loanOwner, string loanAmount, string houseID, string houseType,string housePurchaseDate, string houseObjectType,
            string houseProjectName, string houseProjectNumber, string houseAddressStreet, string houseAddressNumber, string houesAddressPostalCode, string houseAddressCity, string houseAddressMunicipality,
            string houseGroundPrice, string houseExtraWorkContractor, string houseContractorSum,string houseOverDueMaintance, string houseEconomicValue, string houseEconomicValueAfterImprovements,
            string leaseHoldID, string leaseHoldType, string leaseHoldValue, string leaseHoldEndDate, string fcMarketValueReport, string fcMortgageRegistration, string fcAdviceFee,
            string fcMortgageDeed, string fcStartDate, string fcMortgageTotal, string loanPartsID, string loanpartsType, string loanpartsEndDate, string loanpartsValue,string loanpartsFixedInterestDuration,
            string tliInitialValue, string tliInsuredValue, string adviceStatus, string applicationType)
        {
            string JSONString;            

            #region person
            JSONString = "{";
            JSONString = JSONString + Environment.NewLine + @"""ApplicationID""" + ":" + @"""" + personID + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"""Application""" + ":" + " {";
            JSONString = JSONString + Environment.NewLine + @"      ""Inventory""" + ":" + " {";
            JSONString = JSONString + Environment.NewLine + @"          ""Persons""" + ":" + " [";
            JSONString = JSONString + Environment.NewLine + "           {";
            JSONString = JSONString + Environment.NewLine + @"              ""Type""" + ":" + @"""" + personType + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""ID""" + ":" + @"""" + personID + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Initials""" + ":" + @"""" + personInitials  + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""FirstName""" + ":" + @"""" + personFirstName + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""SurName""" + ":" + @"""" + personSurName + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Prefix""" + ":" + @"""" + personPrefix + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""BirthSurName""" + ":" + @"""" + personBirthSurname + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Email""" + ":" + @"""" + Email + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Gender""" + ":" + @"""" + Gender + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""DateOfBirth""" + ":" + @"""" + DateOfBirth + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""MaritalStatus""" + ":" + @"""" + MaritalStatus + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""EverDivorced""" + ":" + @"""" + EverDivorced + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""DivorceDate""" + ":" + @"""" + DivorceDate + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""SocialSecurityNumber""" + ":" + @"""" + SocialSecurityNumber + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""DiscountAOWYears""" + ":" + DiscountAOWYears + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Smoker""" + ":" + Smoker + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Nationality""" + ":" + @"""" + Nationality + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Address""" + ": {";
            JSONString = JSONString + Environment.NewLine + @"                  ""Street""" + ":" + @"""" + addressStreet + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"                  ""HouseNumber""" + ":" + @"""" + addressHouseNumber + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"                  ""PostalCode""" + ":" + @"""" + addressPostalCode + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"                  ""City""" + ":" + @"""" + addressCity + @"""";
            JSONString = JSONString + Environment.NewLine + "               }";
            JSONString = JSONString + Environment.NewLine + "           }";
            JSONString = JSONString + Environment.NewLine + "       ],";

            #endregion

            #region incomes
            JSONString = JSONString + Environment.NewLine + @"          ""Incomes""" + ":" + " [";
            JSONString = JSONString + Environment.NewLine + "           {";
            JSONString = JSONString + Environment.NewLine + @"              ""ID""" + ":" + @"""" + incomesID + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Type""" + ":" + @"""" + incomesType + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"             ""Owner""" + ":" + @"""" + incomesOwner + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""StartDate""" + ":" + @"""" + incomesStartDate + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""EndDate""" + ":" + @"""" + incomesEndDate + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""PartTimePercentage""" + ":" + incomesPartTimePercentage + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""YearlyIncomes""" + ":  [{";
            JSONString = JSONString + Environment.NewLine + @"              ""Year""" + ":" + yearlyIncomesYear + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Value""" + ":" + yearlyIncomesValue + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""HolidayAllowanceValue""" + ":" + yearlyIncomesHolidayAllowanceValue + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""IrregularityPayment""" + ":" + yearlyIncomesIrregularityPayment + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""ExtraMonth""" + ":" + yearlyIncomesExtraMonth + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""ProvisionValue""" + ":" + yearlyIncomesProvisionValue + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""EndOfYearBenefitValue""" + ":" + yearlyIncomesEndOfYearBenefitValue + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""OverTimeValue""" + ":" + yearlyIncomesOverTimeValue;
            JSONString = JSONString + Environment.NewLine + "           }]";
            JSONString = JSONString + Environment.NewLine + "        }";
            JSONString = JSONString + Environment.NewLine + "      ],";
            #endregion

            #region Expenses
            JSONString = JSONString + Environment.NewLine + @"          ""Expenses""" + ":" + " [";
            JSONString = JSONString + Environment.NewLine + "           {";
            JSONString = JSONString + Environment.NewLine + @"              ""ID""" + ":" + @"""" + expensesID + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Type""" + ":" + @"""" + expensesType + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Owner""" + ":" + @"""" + expensesOwner + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""ExpenseMade""" + ":" + ExpenseMade;
            JSONString = JSONString + Environment.NewLine + "           }";
            JSONString = JSONString + Environment.NewLine + "       ],";
            #endregion

            #region loans
            JSONString = JSONString + Environment.NewLine + @"          ""Loans""" + ":" + " [";
            JSONString = JSONString + Environment.NewLine + "           {";
            JSONString = JSONString + Environment.NewLine + @"              ""ID""" + ":" + @"""" + loanID + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Type""" + ":" + @"""" + loanType + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Owner""" + ":" + @"""" + loanOwner + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Value""" + ":" + loanAmount;
            JSONString = JSONString + Environment.NewLine + "           }";
            JSONString = JSONString + Environment.NewLine + "       ],";
            #endregion

            #region Assets
            JSONString = JSONString + Environment.NewLine + @"          ""Assets""" + ":" + " [";
            JSONString = JSONString + Environment.NewLine + "           {";
            JSONString = JSONString + Environment.NewLine + @"              ""ID""" + ":" + @"""" + assetID + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"             ""Type""" + ":" + @"""" + assetType + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Owner""" + ":" + @"""" + assetOwner + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Value""" + ":" + assetValue + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""InterestRate""" + ":" + assetInterestRate;
            JSONString = JSONString + Environment.NewLine + "           }";
            JSONString = JSONString + Environment.NewLine + "       ],";
            #endregion

            #region Pension
            JSONString = JSONString + Environment.NewLine + @"          ""Pensions""" + ":" + " [";
            JSONString = JSONString + Environment.NewLine + "           {";
            JSONString = JSONString + Environment.NewLine + @"              ""ID""" + ":" + @"""" + pensionID + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Type""" + ":" + @"""" + pensionType + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Owner""" + ":" + @"""" + pensionOwner + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""StartDate""" + ":" + @"""" + pensionStartDate + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Value""" + ":" + pensionValue;
            JSONString = JSONString + Environment.NewLine + "           }";
            JSONString = JSONString + Environment.NewLine + "       ],";
            #endregion

            #region RealpropertyHouse
            if (houseType == "NewlyBuiltHouse")
            {
                
                JSONString = JSONString + Environment.NewLine + @"""RealProperty""" + ":" + " {";
                JSONString = JSONString + Environment.NewLine + @"          ""House""" + ":";
                JSONString = JSONString + Environment.NewLine + "           {";
                JSONString = JSONString + Environment.NewLine + @"              ""ID""" + ":" + @"""" + houseID + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""HouseType""" + ":" + @"""" + houseType + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""PurchaseDate""" + ":" + @"""" + housePurchaseDate + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""ObjectType""" + ":" + @"""" + houseObjectType + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""ProjectName""" + ":" + @"""" + houseProjectName + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""ProjectNumber""" + ":" + @"""" + houseProjectNumber + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"          ""Address""" + ":" + " {";
                JSONString = JSONString + Environment.NewLine + @"              ""Street""" + ":" + @"""" + houseAddressStreet + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""HouseNumber""" + ":" + @"""" + houseAddressHouseNumber + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""PostalCode""" + ":" + @"""" + houseAddressPostalCode + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""City""" + ":" + @"""" + houseAddressCity + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""Municipality""" + ":" + @"""" + houseAddressMunicipality + @"""";
                JSONString = JSONString + Environment.NewLine + "           },";
                JSONString = JSONString + Environment.NewLine + @"              ""GroundPrice""" + ":" + houseGroundPrice + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""ExtraWorkContractor""" + ":" + houseExtraWorkContractor + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""ContractorSum""" + ":" + houseContractorSum;
                JSONString = JSONString + Environment.NewLine + "           },";
            }

            else if (houseType == "ExistingHouse")
            {
                #region RealpropertyHouse
                JSONString = JSONString + Environment.NewLine + @"""RealProperty""" + ":" + " {";
                JSONString = JSONString + Environment.NewLine + @"          ""House""" + ":";
                JSONString = JSONString + Environment.NewLine + "           {";
                JSONString = JSONString + Environment.NewLine + @"              ""ID""" + ":" + @"""" + houseID + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""HouseType""" + ":" + @"""" + houseType + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""PurchaseDate""" + ":" + @"""" + housePurchaseDate + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""ObjectType""" + ":" + @"""" + houseObjectType + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"          ""Address""" + ":" + " {";
                JSONString = JSONString + Environment.NewLine + @"              ""Street""" + ":" + @"""" + houseAddressStreet + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""HouseNumber""" + ":" + @"""" + houseAddressHouseNumber + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""PostalCode""" + ":" + @"""" + houseAddressPostalCode + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""City""" + ":" + @"""" + houseAddressCity + @"""" + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""Municipality""" + ":" + @"""" + houseAddressMunicipality + @"""";
                JSONString = JSONString + Environment.NewLine + "           },";
                JSONString = JSONString + Environment.NewLine + @"              ""OverDueMaintenance""" + ":" + houseOverDueMaintenance + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""EconomicValue""" + ":" + houseEconomicValue + ",";
                JSONString = JSONString + Environment.NewLine + @"              ""EconomicValueAfterImprovements""" + ":" + houseEconomicValueAfterImprovements;
                JSONString = JSONString + Environment.NewLine + "           },";
            }



            #endregion

            #region Realproperty Leasehold

            JSONString = JSONString + Environment.NewLine + @"  ""LeaseHold""" + ":" + " {";
            JSONString = JSONString + Environment.NewLine + @"      ""ID""" + ":" + @"""" + leaseHoldID + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"      ""Type""" + ":" + @"""" + leaseHoldType + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"      ""Value""" + ":" + leaseHoldValue + ",";
            JSONString = JSONString + Environment.NewLine + @"      ""EndDate""" + ":" + @"""" + leaseHoldEndDate + @"""" + "";
            JSONString = JSONString + Environment.NewLine + "   }";
            JSONString = JSONString + Environment.NewLine + "},";
            #endregion

            #endregion

            #region Financeconstruction
            JSONString = JSONString + Environment.NewLine + @"  ""FinanceConstruction""" + ":" + " {";
            JSONString = JSONString + Environment.NewLine + @"      ""MarketValueReport""" + ":" + fcMarketValueReport + ",";
            JSONString = JSONString + Environment.NewLine + @"      ""MortgageRegistration""" + ":" + fcMortgageRegistration + ",";
            JSONString = JSONString + Environment.NewLine + @"      ""AdviceFee""" + ":" + fcAdviceFee + ",";
            JSONString = JSONString + Environment.NewLine + @"      ""MortgageDeed""" + ":" + fcMortgageDeed + ",";
            JSONString = JSONString + Environment.NewLine + @"      ""StartDate""" + ":" + @"""" + fcStartDate + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"      ""MortgageTotal""" + ":" + fcMortgageTotal + ",";
            #endregion

            #region Loanparts
            JSONString = JSONString + Environment.NewLine + @"          ""LoanParts""" + ":" + " [{";
            JSONString = JSONString + Environment.NewLine + @"              ""ID""" + ":" + @"""" + loanpartsID + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Type""" + ":" + @"""" + loanpartsType + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""EndDate""" + ":" + @"""" + loanpartsEndDate + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Value""" + ":" + loanpartsValue + ",";
            JSONString = JSONString + Environment.NewLine + @"              ""Fixed Interesat Duration""" + ":" + loanpartsFixedInterestDuration;
            JSONString = JSONString + Environment.NewLine + "   }";
            JSONString = JSONString + Environment.NewLine + "],";
            #endregion

            #region termlife insurances
            JSONString = JSONString + Environment.NewLine + @"          ""TermLifeInsurances""" + ":" + " [{";
            JSONString = JSONString + Environment.NewLine + @"              ""Premium""" + ":";
            JSONString = JSONString + Environment.NewLine + "               {";
            JSONString = JSONString + Environment.NewLine + @"              ""InitialValue""" + ":" + @"""" + tliInitialValue + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + "               }";
            JSONString = JSONString + Environment.NewLine + @"              ""TermlifeCoverage""" + ":";
            JSONString = JSONString + Environment.NewLine + "               {";
            JSONString = JSONString + Environment.NewLine + @"              ""InsuredValue""" + ":" + @"""" + tliInsuredValue + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + "               }";
            //JSONString = JSONString + Environment.NewLine + @"              ""ID""" + ":" + @"""" + tliID + @"""" + ",";
            //JSONString = JSONString + Environment.NewLine + @"              ""Owner""" + ":" + @"""" + tliOwner + @"""" + ",";
            //JSONString = JSONString + Environment.NewLine + @"              ""Payment""" + ":" + @"""" + tliPayment + @"""" + ",";
            //JSONString = JSONString + Environment.NewLine + @"              ""InsuredValue""" + ":" + tliInsuredValue + ",";
            //JSONString = JSONString + Environment.NewLine + @"              ""ForLoanPart""" + ":" + @"""" + tliID + @"""";
            JSONString = JSONString + Environment.NewLine + "   }"; 
            JSONString = JSONString + Environment.NewLine + "]";
            JSONString = JSONString + Environment.NewLine + "}";
            JSONString = JSONString + Environment.NewLine + "},";
            #endregion

            #region Application Details
            JSONString = JSONString + Environment.NewLine + @"  ""ApplicationDetails""" + ":" + " {";
            JSONString = JSONString + Environment.NewLine + @"          ""AdviceStatus""" + ":" + @"""" + adviceStatus + @"""" + ",";
            JSONString = JSONString + Environment.NewLine + @"       ""ApplicationType""" + ":" + @"""" + applicationType + @"""";
            JSONString = JSONString + Environment.NewLine + "       }";
            JSONString = JSONString + Environment.NewLine + "   }";
            JSONString = JSONString + Environment.NewLine + "}";
            #endregion

            writeToTextFile(JSONString);

        }
        
        #endregion

        #region
        //public void BuildJSONString()
        //{
        //    string JSONString;

        //    JSONString = "{";
        //    JSONString = JSONString + Environment.NewLine + @"ApplicationID" + ":" + "" + applicationID + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Application" + ":" + " {";
        //    JSONString = JSONString + Environment.NewLine + @"Inventory" + ":" + " {";
        //    JSONString = JSONString + Environment.NewLine + @"Persons" + ":" + " [";
        //    JSONString = JSONString + Environment.NewLine + "{";
        //    JSONString = JSONString + Environment.NewLine + @"Type" + ":" + "" + personType + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"ID" + ":" + "" + personID + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Initials" + ":" + "" + personInitials + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"FirstName" + ":" + "" + personFirstName + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"SurName" + ":" + "" + personSurName + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Prefix" + ":" + "" + personPrefix + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"BirthSurName" + ":" + "" + personBirthSurname + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Email" + ":" + "" + Email + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Gender" + ":" + "" + Gender + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"DateOfBirth" + ":" + "" + DateOfBirth + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"MaritalStatus" + ":" + "" + MaritalStatus + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"EverDivorced" + ":" + "" + EverDivorced + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"DivorceDate" + ":" + "" + DivorceDate + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"SocialSecurityNumber" + ":" + "" + SocialSecurityNumber + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"DiscountAOWYears" + ":" + DiscountAOWYears + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Smoker" + ":" + "" + Smoker + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Nationality" + ":" + "" + Nationality + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Address" + ": {";
        //    JSONString = JSONString + Environment.NewLine + @"  Street" + ":" + "" + addressStreet + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"  HouseNumber" + ":" + "" + addressHouseNumber + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"  PostalCode" + ":" + "" + addressPostalCode + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"  City" + ":" + "" + addressCity + "";
        //    JSONString = JSONString + Environment.NewLine + "}";
        //    JSONString = JSONString + Environment.NewLine + "}";
        //    JSONString = JSONString + Environment.NewLine + "],";

        //    JSONString = JSONString + Environment.NewLine + @"Incomes" + ":" + " [";
        //    JSONString = JSONString + Environment.NewLine + "{";
        //    JSONString = JSONString + Environment.NewLine + @"ID" + ":" + "" + incomesID + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Type" + ":" + "" + incomesType + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Owner" + ":" + "" + incomesOwner + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"DateOfBirth" + ":" + "" + incomesDateofBirth + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"StartDate" + ":" + "" + incomesStartDate + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"EndDate" + ":" + "" + incomesEndDate + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"PartTimePercentage" + ":" + incomesPartTimePercentage + ",";
        //    JSONString = JSONString + Environment.NewLine + @"YearlyIncomes" + ":  [{";
        //    JSONString = JSONString + Environment.NewLine + @"Year" + ":" + yearlyIncomesYear + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Value" + ":" + yearlyIncomesValue + ",";
        //    JSONString = JSONString + Environment.NewLine + @"HolidayAllowanceValue" + ":" + yearlyIncomesHolidayAllowanceValue + ",";
        //    JSONString = JSONString + Environment.NewLine + @"IrregularityPayment" + ":" + yearlyIncomesIrregularityPayment + ",";
        //    JSONString = JSONString + Environment.NewLine + @"ExtraMonth" + ":" + yearlyIncomesExtraMonth + ",";
        //    JSONString = JSONString + Environment.NewLine + @"ProvisionValue" + ":" + yearlyIncomesProvisionValue + ",";
        //    JSONString = JSONString + Environment.NewLine + @"EndOfYearBenefitValue" + ":" + yearlyIncomesEndOfYearBenefitValue + ",";
        //    JSONString = JSONString + Environment.NewLine + @"OverTimeValue" + ":" + yearlyIncomesOverTimeValue;
        //    JSONString = JSONString + Environment.NewLine + "]}";
        //    JSONString = JSONString + Environment.NewLine + "}";
        //    JSONString = JSONString + Environment.NewLine + "],";
        //    JSONString = JSONString + Environment.NewLine + @"Expenses" + ":" + " [";
        //    JSONString = JSONString + Environment.NewLine + "{";
        //    JSONString = JSONString + Environment.NewLine + @"ID" + ":" + "" + expensesID + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Type" + ":" + "" + expensesType + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Owner" + ":" + "" + expensesOwner + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"ExpenseMade" + ":" + expensesExpenseMade;
        //    JSONString = JSONString + Environment.NewLine + "}";
        //    JSONString = JSONString + Environment.NewLine + "],";

        //    JSONString = JSONString + Environment.NewLine + @"Loans" + ":" + " [";
        //    JSONString = JSONString + Environment.NewLine + "{";
        //    JSONString = JSONString + Environment.NewLine + @"ID" + ":" + "" + loanID + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Type" + ":" + "" + loanType + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Owner" + ":" + "" + loanOwner + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Amount" + ":" + loanAmount;
        //    JSONString = JSONString + Environment.NewLine + "}";
        //    JSONString = JSONString + Environment.NewLine + "],";

        //    JSONString = JSONString + Environment.NewLine + @"Assets" + ":" + " [";
        //    JSONString = JSONString + Environment.NewLine + "{";
        //    JSONString = JSONString + Environment.NewLine + @"ID" + ":" + "" + assetID + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Type" + ":" + "" + assetType + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Owner" + ":" + "" + assetOwner + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Value" + ":" + assetValue;
        //    JSONString = JSONString + Environment.NewLine + @"InterestRate" + ":" + assetInterestRate;
        //    JSONString = JSONString + Environment.NewLine + "}";
        //    JSONString = JSONString + Environment.NewLine + "],";

        //    JSONString = JSONString + Environment.NewLine + @"Pension" + ":" + " [";
        //    JSONString = JSONString + Environment.NewLine + "{";
        //    JSONString = JSONString + Environment.NewLine + @"ID" + ":" + "" + pensionID + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Type" + ":" + "" + pensionType + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Owner" + ":" + "" + pensionOwner + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"StartDate" + ":" + "" + pensionStartDate + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"Value" + ":" + pensionValue;
        //    JSONString = JSONString + Environment.NewLine + "}";
        //    JSONString = JSONString + Environment.NewLine + "],";

        //    JSONString = JSONString + Environment.NewLine + @"RealProperty" + ":" + " {";
        //    JSONString = JSONString + Environment.NewLine + @"House" + ":";
        //    JSONString = JSONString + Environment.NewLine + "{";
        //    JSONString = JSONString + Environment.NewLine + @"ID" + ":" + "" + houseID + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"HouseType" + ":" + "" + houseType + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"PurchaseDate" + ":" + "" + housePurchaseDate + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"ObjectType" + ":" + "" + houseObjectType + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"ProjectName" + ":" + "" + houseProjectName + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"ProjectNumber" + ":" + "" + houseProjectNumber + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"  Address" + ":" + " {";
        //    JSONString = JSONString + Environment.NewLine + @"      Street" + ":" + "" + houseAddressStreet + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"      HouseNumber" + ":" + houseAddressHouseNumber + ",";
        //    JSONString = JSONString + Environment.NewLine + @"      PostalCode" + ":" + "" + houseAddressPostalCode + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"      City" + ":" + "" + houseAddressCity + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"      Municipality" + ":" + "" + houseAddressMunicipality;
        //    JSONString = JSONString + Environment.NewLine + "},";
        //    JSONString = JSONString + Environment.NewLine + @"GroundPrice" + ":" + houseGroundPrice + ",";
        //    JSONString = JSONString + Environment.NewLine + @"ExtraWorkContractor" + ":" + houseExtraWorkContractor + ",";
        //    JSONString = JSONString + Environment.NewLine + @"ContractorSum" + ":" + houseContractorSum + ",";
        //    JSONString = JSONString + Environment.NewLine + @"OverDueMaintenance" + ":" + houseOverDueMaintenance + ",";
        //    JSONString = JSONString + Environment.NewLine + @"EconomicValue" + ":" + houseEconomicValue + ",";
        //    JSONString = JSONString + Environment.NewLine + @"EconomicValueAfterImprovements" + ":" + houseEconomicValueAfterImprovements;
        //    JSONString = JSONString + Environment.NewLine + "},";

        //    JSONString = JSONString + Environment.NewLine + @"  LeaseHold" + ":" + " {";
        //    JSONString = JSONString + Environment.NewLine + @"      ID" + ":" + "" + leaseHoldID + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"      Type" + ":" + "" + leaseHoldType + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"      Value" + ":" + leaseHoldValue + ",";
        //    JSONString = JSONString + Environment.NewLine + @"      EndDate" + ":" + "" + leaseHoldEndDate + "" + "";
        //    JSONString = JSONString + Environment.NewLine + "   }";
        //    JSONString = JSONString + Environment.NewLine + "},";

        //    JSONString = JSONString + Environment.NewLine + @"  FinanceConstruction" + ":" + " {";
        //    JSONString = JSONString + Environment.NewLine + @"      MarketValueReport" + ":" + fcMarketValueReport + ",";
        //    JSONString = JSONString + Environment.NewLine + @"      MortgageRegistration" + ":" + fcMortgageRegistration + ",";
        //    JSONString = JSONString + Environment.NewLine + @"      AdviceFee" + ":" + fcAdviceFee + ",";
        //    JSONString = JSONString + Environment.NewLine + @"      MortgageDeed" + ":" + fcMortgageDeed + ",";
        //    JSONString = JSONString + Environment.NewLine + @"      StartDate" + ":" + "" + fcStartDate + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"      MortgageTotal" + ":" + fcMortgageTotal + ",";
        //    JSONString = JSONString + Environment.NewLine + @"          LoanParts" + ":" + " [{";
        //    JSONString = JSONString + Environment.NewLine + @"              Type" + ":" + "" + loanpartsType + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"              EndDate" + ":" + "" + loanpartsEndDate + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"              Value" + ":" + loanpartsValue + ",";
        //    JSONString = JSONString + Environment.NewLine + @"              Value" + ":" + loanpartsFixedInterestDuration + ",";
        //    JSONString = JSONString + Environment.NewLine + "   }";
        //    JSONString = JSONString + Environment.NewLine + "],";

        //    JSONString = JSONString + Environment.NewLine + @"  TermLifeInsurances" + ":" + " [{";
        //    JSONString = JSONString + Environment.NewLine + @"      Premium: ";
        //    JSONString = JSONString + Environment.NewLine + "{";
        //    JSONString = JSONString + Environment.NewLine + @"      InitialValue" + ":" + TLITermlifeCoverageInsuredValue;
        //    JSONString = JSONString + Environment.NewLine + "}";
        //    JSONString = JSONString + Environment.NewLine + @"      TermLifeCoverage: ";
        //    JSONString = JSONString + Environment.NewLine + "{";
        //    JSONString = JSONString + Environment.NewLine + @"      InsuredValue" + ":" + TLITermlifeCoverageInsuredValue;
        //    JSONString = JSONString + Environment.NewLine + "}";
        //    JSONString = JSONString + Environment.NewLine + "}";
        //    JSONString = JSONString + Environment.NewLine + "]";
        //    JSONString = JSONString + Environment.NewLine + "}";
        //    JSONString = JSONString + Environment.NewLine + "},";
        //    JSONString = JSONString + Environment.NewLine + @"  ApplicationDetails" + ":" + " {";
        //    JSONString = JSONString + Environment.NewLine + @"      AdviceStatus" + ":" + "" + adviceStatus + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + @"      ApplicationType" + ":" + "" + applicationType + "" + ",";
        //    JSONString = JSONString + Environment.NewLine + "}";
        //    JSONString = JSONString + Environment.NewLine + "}";


        //    writeToTextFile(JSONString);
        //}
        #endregion

    }
}
